/**
 * @EnumName Polite
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 16:14
 * @Version 1.0
 **/
package com.test.aop;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Polite {

}
