/**
 * @ClassName AopTest
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 16:09
 * @Version 1.0
 **/

package com.test.aop;

import com.study.spring.aop.ProxyResolver;

public class AopTest {
    public static void main(String[] args) {
        // 原始Bean:
        OriginBean origin = new OriginBean();
        origin.name = "Bob";
        // 调用原始Bean的hello():
        System.out.println("Hello, Bob.".equals(origin.hello()));

        // 创建Proxy:
        OriginBean proxy = new ProxyResolver().createProxy(origin, new PoliteInvocationHandler());

        // Proxy类名,类似OriginBean$ByteBuddy$9hQwRy3T:
        System.out.println(proxy.getClass().getName());

        // Proxy类与OriginBean.class不同:
        System.out.println(OriginBean.class == proxy.getClass());
        // proxy实例的name字段应为null:
        System.out.println(proxy.name == null );
        // 调用带@Polite的方法:
        System.out.println("Hello, Bob!".equals(proxy.hello()));
        // 调用不带@Polite的方法:
        System.out.println("Morning, Bob.".equals(proxy.morning()));
    }

}
