/**
 * @ClassName OtherBean
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 17:28
 * @Version 1.0
 **/

package com.test.aop.around;

import com.study.spring.beanDefinition.Autowired;
import com.study.spring.beanDefinition.Component;
import com.study.spring.beanDefinition.Order;

@Order(0)
@Component
public class OtherBean {

    public OriginBean origin;

    public OtherBean(@Autowired OriginBean origin) {
        this.origin = origin;
    }
}
