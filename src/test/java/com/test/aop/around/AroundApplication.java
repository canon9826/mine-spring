/**
 * @ClassName AroundApplication
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 17:24
 * @Version 1.0
 **/

package com.test.aop.around;

import com.study.spring.around.AroundProxyBeanPostProcessor;
import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.ComponentScan;
import com.study.spring.beanDefinition.Configuration;

@Configuration
@ComponentScan//({"com.study.spring","com.test.aop"})
public class AroundApplication {

    @Bean
    AroundProxyBeanPostProcessor createAroundProxyBeanPostProcessor() {
        return new AroundProxyBeanPostProcessor();
    }
}
