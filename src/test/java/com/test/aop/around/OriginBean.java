/**
 * @ClassName OriginBean
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 17:47
 * @Version 1.0
 **/

package com.test.aop.around;

import com.study.spring.around.Around;
import com.study.spring.beanDefinition.Component;
import com.study.spring.beanDefinition.Value;
import com.test.aop.Polite;

@Component
@Around("aroundInvocationHandler")
public class OriginBean {

    @Value("${customer.name}")
    public String name;

    @Polite
    public String hello() {
        return "Hello, " + name + ".";
    }

    public String morning() {
        return "Morning, " + name + ".";
    }
}

