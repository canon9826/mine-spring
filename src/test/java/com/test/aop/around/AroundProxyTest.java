/**
 * @ClassName AroundTest
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 17:25
 * @Version 1.0
 **/

package com.test.aop.around;

import com.study.spring.beanDefinition.AnnotationConfigApplicationContext;
import com.study.spring.property.PropertyResolver;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class AroundProxyTest {
    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AroundApplication.class, createPropertyResolver())) {
            OriginBean proxy = ctx.getBean(OriginBean.class);
            // OriginBean$ByteBuddy$8NoD1FcQ
            System.out.println(proxy.getClass().getName());

            // proxy class, not origin class:
            assertNotSame(OriginBean.class, proxy.getClass());
            // proxy.name not injected:
            assertNull(proxy.name);

            assertEquals("Hello, Bob!", proxy.hello());
            assertEquals("Morning, Bob.", proxy.morning());

            // test injected proxy:
            OtherBean other = ctx.getBean(OtherBean.class);
            assertSame(proxy, other.origin);
            assertEquals("Hello, Bob!", other.origin.hello());
        }


    }
    public static PropertyResolver createPropertyResolver() {
        Properties ps = new Properties();
        ps.put("customer.name", "Bob");
        PropertyResolver pr = new PropertyResolver(ps);
        return pr;
    }


}
