/**
 * @ClassName OriginBean
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 16:13
 * @Version 1.0
 **/

package com.test.aop;

public class OriginBean {
    public String name;

    @Polite
    public String hello() {
        return "Hello, " + name + ".";
    }

    public String morning() {
        return "Morning, " + name + ".";
    }
}
