package com.test.ioc.scan;

import com.test.ioc.imported.LocalDateConfiguration;
import com.test.ioc.imported.ZonedDateConfiguration;
import com.study.spring.beanDefinition.ComponentScan;
import com.study.spring.beanDefinition.Import;


@ComponentScan
@Import({ LocalDateConfiguration.class, ZonedDateConfiguration.class })
public class ScanApplication {

}
