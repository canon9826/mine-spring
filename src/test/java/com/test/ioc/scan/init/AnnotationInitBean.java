package com.test.ioc.scan.init;


import com.study.spring.beanDefinition.Component;
import com.study.spring.beanDefinition.Value;

import javax.annotation.PostConstruct;

@Component
public class AnnotationInitBean {

    @Value("${app.title}")
    String appTitle;

    @Value("${app.version}")
    String appVersion;

    public String appName;

    @PostConstruct
    void init() {
        this.appName = this.appTitle + " / " + this.appVersion;
    }
}
