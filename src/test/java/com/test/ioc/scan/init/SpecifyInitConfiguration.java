package com.test.ioc.scan.init;


import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.Configuration;
import com.study.spring.beanDefinition.Value;

@Configuration
public class SpecifyInitConfiguration {

    @Bean(initMethod = "init")
    SpecifyInitBean createSpecifyInitBean(@Value("${app.title}") String appTitle, @Value("${app.version}") String appVersion) {
        return new SpecifyInitBean(appTitle, appVersion);
    }
}
