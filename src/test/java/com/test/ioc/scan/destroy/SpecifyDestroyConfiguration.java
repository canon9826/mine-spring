package com.test.ioc.scan.destroy;


import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.Configuration;
import com.study.spring.beanDefinition.Value;

@Configuration
public class SpecifyDestroyConfiguration {

    @Bean(destroyMethod = "destroy")
    SpecifyDestroyBean createSpecifyDestroyBean(@Value("${app.title}") String appTitle) {
        return new SpecifyDestroyBean(appTitle);
    }
}
