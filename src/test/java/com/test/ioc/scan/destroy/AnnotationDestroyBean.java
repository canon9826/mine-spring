package com.test.ioc.scan.destroy;


import com.study.spring.beanDefinition.Component;
import com.study.spring.beanDefinition.Value;

import javax.annotation.PreDestroy;

@Component
public class AnnotationDestroyBean {

    @Value("${app.title}")
    public String appTitle;

    @PreDestroy
    void destroy() {
        this.appTitle = null;
    }
}
