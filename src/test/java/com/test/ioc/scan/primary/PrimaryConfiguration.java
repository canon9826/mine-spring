package com.test.ioc.scan.primary;


import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.Configuration;
import com.study.spring.beanDefinition.Primary;

@Configuration
public class PrimaryConfiguration {

    @Primary
    @Bean
    DogBean husky() {
        return new DogBean("Husky");
    }

    @Bean
    DogBean teddy() {
        return new DogBean("Teddy");
    }
}
