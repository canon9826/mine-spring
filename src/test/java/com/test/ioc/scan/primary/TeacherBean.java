package com.test.ioc.scan.primary;


import com.study.spring.beanDefinition.Component;
import com.study.spring.beanDefinition.Primary;

@Primary
@Component
public class TeacherBean extends PersonBean {

}
