package com.test.ioc.scan.nested;


import com.study.spring.beanDefinition.Component;

@Component
public class OuterBean {

    @Component
    public static class NestedBean {

    }
}
