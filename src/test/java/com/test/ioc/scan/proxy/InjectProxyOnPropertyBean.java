package com.test.ioc.scan.proxy;


import com.study.spring.beanDefinition.Autowired;
import com.study.spring.beanDefinition.Component;

@Component
public class InjectProxyOnPropertyBean {

    @Autowired
    public OriginBean injected;
}
