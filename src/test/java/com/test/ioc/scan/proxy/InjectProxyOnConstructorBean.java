package com.test.ioc.scan.proxy;


import com.study.spring.beanDefinition.Autowired;
import com.study.spring.beanDefinition.Component;

@Component
public class InjectProxyOnConstructorBean {

    public final OriginBean injected;

    public InjectProxyOnConstructorBean(@Autowired OriginBean injected) {
        this.injected = injected;
    }
}
