package com.test.ioc.imported;

import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;



@Configuration
public class LocalDateConfiguration {

    @Bean
    LocalDate startLocalDate() {
        return LocalDate.now();
    }

    @Bean
    LocalDateTime startLocalDateTime() {
        return LocalDateTime.now();
    }
}
