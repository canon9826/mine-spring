package com.test.ioc.imported;

import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.Configuration;

import java.time.ZonedDateTime;



@Configuration
public class ZonedDateConfiguration {

    @Bean
    ZonedDateTime startZonedDateTime() {
        return ZonedDateTime.now();
    }
}
