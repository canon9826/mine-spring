package com.test.ioc.summer.context;

import com.study.spring.beanDefinition.AnnotationConfigApplicationContext;
import com.study.spring.property.PropertyResolver;
import com.test.ioc.imported.LocalDateConfiguration;
import com.test.ioc.imported.ZonedDateConfiguration;
import com.test.ioc.scan.ScanApplication;
import com.test.ioc.scan.convert.ValueConverterBean;
import com.test.ioc.scan.custom.annotation.CustomAnnotationBean;
import com.test.ioc.scan.destroy.AnnotationDestroyBean;
import com.test.ioc.scan.destroy.SpecifyDestroyBean;
import com.test.ioc.scan.init.AnnotationInitBean;
import com.test.ioc.scan.init.SpecifyInitBean;
import com.test.ioc.scan.nested.OuterBean;
import com.test.ioc.scan.primary.DogBean;
import com.test.ioc.scan.primary.PersonBean;
import com.test.ioc.scan.primary.TeacherBean;
import com.test.ioc.scan.proxy.InjectProxyOnConstructorBean;
import com.test.ioc.scan.proxy.InjectProxyOnPropertyBean;
import com.test.ioc.scan.proxy.OriginBean;
import com.test.ioc.scan.proxy.SecondProxyBean;
import com.test.ioc.scan.sub1.Sub1Bean;
import com.test.ioc.scan.sub1.sub2.Sub2Bean;
import com.test.ioc.scan.sub1.sub2.sub3.Sub3Bean;

import java.time.*;
import java.util.Properties;

public class AnnotationConfigApplicationContextTest {

    public static void main(String[] args) {
        //region BeanDefinition test

        //AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ScanApplication.class, createPropertyResolver());
        //// @CustomAnnotation:
        //System.out.println(ctx.findBeanDefinition(CustomAnnotationBean.class).toString());
        //System.out.println(ctx.findBeanDefinition("customAnnotation").toString());;
        //
        //// @Import():
        //System.out.println(ctx.findBeanDefinition(LocalDateConfiguration.class).toString());;
        //System.out.println(ctx.findBeanDefinition("startLocalDate").toString());;
        //System.out.println(ctx.findBeanDefinition("startLocalDateTime").toString());;
        //System.out.println(ctx.findBeanDefinition(ZonedDateConfiguration.class).toString());;
        //System.out.println(ctx.findBeanDefinition("startZonedDateTime").toString());;
        //// nested:
        //System.out.println(ctx.findBeanDefinition(OuterBean.class).toString());;
        //System.out.println(ctx.findBeanDefinition(OuterBean.NestedBean.class).toString());;
        //
        //BeanDefinition studentDef = ctx.findBeanDefinition(StudentBean.class);
        //BeanDefinition teacherDef = ctx.findBeanDefinition(TeacherBean.class);
        //// 2 PersonBean:res->person,student,teacher
        //List<BeanDefinition> defs = ctx.findBeanDefinitions(PersonBean.class);
        //System.out.println(studentDef.equals(defs.get(0)));
        //System.out.println(teacherDef.equals(defs.get(1)));
        //// 1 @Primary PersonBean:
        //BeanDefinition personPrimaryDef = ctx.findBeanDefinition(PersonBean.class);
        //System.out.println(teacherDef == personPrimaryDef);

        //endregion

        //region createBean test
        //AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ScanApplication.class, createPropertyResolver());
        //System.out.println(ctx.getBean(CustomAnnotationBean.class).toString());
        //System.out.println(ctx.getBean("customAnnotation").toString());
        //
        //System.out.println(ctx.getBean(LocalDateConfiguration.class).toString());
        //System.out.println(ctx.getBean("startLocalDate").toString());
        //System.out.println(ctx.getBean("startLocalDateTime").toString());
        //System.out.println(ctx.getBean(ZonedDateConfiguration.class).toString());
        //System.out.println(ctx.getBean("startZonedDateTime").toString());
        //
        //ctx.getBean(OuterBean.class);
        //ctx.getBean(OuterBean.NestedBean.class);
        //
        //PersonBean person = ctx.getBean(PersonBean.class);
        //System.out.println(TeacherBean.class == person.getClass() );
        //DogBean dog = ctx.getBean(DogBean.class);
        //System.out.println("Husky" == dog.type);
        //
        //ctx.getBean(Sub1Bean.class);
        //ctx.getBean(Sub2Bean.class);
        //ctx.getBean(Sub3Bean.class);

        //endregion

        //region proxy
        //AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ScanApplication.class, createPropertyResolver());
        //OriginBean proxy = ctx.getBean(OriginBean.class);
        //System.out.println(SecondProxyBean.class == proxy.getClass());
        //System.out.println("Scan App".equals(proxy.getName()));
        //System.out.println("v1.0".equals(proxy.getVersion()));
        //// make sure proxy.field is not injected:
        //System.out.println(proxy.name);
        //System.out.println(proxy.version);
        //
        //// other beans are injected proxy instance:
        //InjectProxyOnPropertyBean inject1 = ctx.getBean(InjectProxyOnPropertyBean.class);
        //InjectProxyOnConstructorBean inject2 = ctx.getBean(InjectProxyOnConstructorBean.class);
        //System.out.println(proxy == inject1.injected);
        //System.out.println(proxy == inject2.injected);
        //endregion

        //region ioc

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ScanApplication.class, createPropertyResolver());
        System.out.println(ctx.getBean(CustomAnnotationBean.class));
        System.out.println((Object) ctx.getBean("customAnnotation"));


        // test @PostConstruct:
        System.out.println("Scan App / v1.0".equals(ctx.getBean(AnnotationInitBean.class).appName));
        System.out.println("Scan App / v1.0".equals(ctx.getBean(SpecifyInitBean.class).appName));


        System.out.println(ctx.getBean(LocalDateConfiguration.class));
        System.out.println((Object) ctx.getBean("startLocalDate"));
        System.out.println((Object) ctx.getBean("startLocalDateTime"));
        System.out.println(ctx.getBean(ZonedDateConfiguration.class));
        System.out.println((Object) ctx.getBean("startZonedDateTime"));


        AnnotationDestroyBean bean1 = null;
        SpecifyDestroyBean bean2 = null;
        // test @PreDestroy:
        bean1 = ctx.getBean(AnnotationDestroyBean.class);
        bean2 = ctx.getBean(SpecifyDestroyBean.class);
        System.out.println("Scan App".equals(bean1.appTitle));
        System.out.println("Scan App".equals(bean2.appTitle));
        System.out.println(bean1.appTitle);
        System.out.println(bean2.appTitle);

        ValueConverterBean bean = ctx.getBean(ValueConverterBean.class);

        System.out.println(bean.injectedBoolean);
        System.out.println(bean.injectedBooleanPrimitive);
        System.out.println(bean.injectedBoolean);

        System.out.println(bean.injectedByte);
        System.out.println(bean.injectedByte.equals((byte) 123));
        System.out.println(bean.injectedBytePrimitive == (byte) 123);

        System.out.println(bean.injectedShort);
        System.out.println(bean.injectedShort.equals((short) 12345));
        System.out.println(bean.injectedShortPrimitive == ((short) 12345));

        System.out.println(bean.injectedInteger);
        System.out.println(bean.injectedInteger.equals(1234567));
        System.out.println(bean.injectedIntPrimitive == (1234567));

        System.out.println(bean.injectedLong);
        System.out.println(bean.injectedLong.equals(123456789_000L));
        System.out.println(bean.injectedLongPrimitive == (123456789_000L));

        System.out.println(bean.injectedFloat);
        //float 32位 1个字符4位,最多8个字符
        System.out.println(bean.injectedFloat.equals(12345.6789F));
        System.out.println(bean.injectedFloat.equals(0.0001F));
        System.out.println(bean.injectedFloatPrimitive == (12345.6789F));
        System.out.println(bean.injectedFloatPrimitive == (0.0001F));

        System.out.println(bean.injectedDouble);
        System.out.println(bean.injectedDouble.equals(123456789.87654321));
        System.out.println(bean.injectedDouble.equals(0.0000001));
        System.out.println(bean.injectedDoublePrimitive == (123456789.87654321));
        System.out.println(bean.injectedDoublePrimitive == (0.0000001));

        System.out.println(bean.injectedLocalDate.equals(LocalDate.parse("2023-03-29")));
        System.out.println(bean.injectedLocalTime.equals(LocalTime.parse("20:45:01")));
        System.out.println(bean.injectedLocalDateTime.equals(LocalDateTime.parse("2023-03-29T20:45:01")));
        System.out.println(bean.injectedZonedDateTime.equals(ZonedDateTime.parse("2023-03-29T20:45:01+08:00[Asia/Shanghai]")));
        System.out.println(bean.injectedDuration.equals(Duration.parse("P2DT3H4M")));
        System.out.println(bean.injectedZoneId.equals(ZoneId.of("Asia/Shanghai")));


        ctx.getBean(OuterBean.class);
        ctx.getBean(OuterBean.NestedBean.class);


        PersonBean person = ctx.getBean(PersonBean.class);
        System.out.println(TeacherBean.class.equals(person.getClass()));
        DogBean dog = ctx.getBean(DogBean.class);


        // test proxy:
        OriginBean proxy = ctx.getBean(OriginBean.class);
        System.out.println(SecondProxyBean.class == proxy.getClass());
        System.out.println("Scan App".equals(proxy.getName()));
        System.out.println("v1.0".equals(proxy.getVersion()));
        // make sure proxy.field is not injected:

        System.out.println(proxy.name);
        System.out.println(proxy.version);

        // other beans are injected proxy instance:
        InjectProxyOnPropertyBean inject1 = ctx.getBean(InjectProxyOnPropertyBean.class);
        InjectProxyOnConstructorBean inject2 = ctx.getBean(InjectProxyOnConstructorBean.class);
        System.out.println(proxy == inject1.injected);
        System.out.println(proxy == inject2.injected);


        ctx.getBean(Sub1Bean.class);
        ctx.getBean(Sub2Bean.class);
        ctx.getBean(Sub3Bean.class);


        //endregion
    }

    static PropertyResolver createPropertyResolver() {
        Properties ps = new Properties();
        ps.put("app.title", "Scan App");
        ps.put("app.version", "v1.0");
        ps.put("jdbc.url", "jdbc:hsqldb:file:testdb.tmp");
        ps.put("jdbc.username", "sa");
        ps.put("jdbc.password", "");
        ps.put("convert.boolean", "true");
        ps.put("convert.byte", "123");
        ps.put("convert.short", "12345");
        ps.put("convert.integer", "1234567");
        ps.put("convert.long", "123456789000");
        ps.put("convert.float", "12345.6789");
        ps.put("convert.double", "123456789.87654321");
        ps.put("convert.localdate", "2023-03-29");
        ps.put("convert.localtime", "20:45:01");
        ps.put("convert.localdatetime", "2023-03-29T20:45:01");
        ps.put("convert.zoneddatetime", "2023-03-29T20:45:01+08:00[Asia/Shanghai]");
        ps.put("convert.duration", "P2DT3H4M");
        ps.put("convert.zoneid", "Asia/Shanghai");
        PropertyResolver pr = new PropertyResolver(ps);
        return pr;
    }


}
