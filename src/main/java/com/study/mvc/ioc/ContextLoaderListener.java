package com.study.mvc.ioc;

import com.study.spring.beanDefinition.AnnotationConfigApplicationContext;
import com.study.spring.beanDefinition.Autowired;
import com.study.spring.ioc.ApplicationContext;
import com.study.spring.property.PropertyResolver;
import com.sun.corba.se.spi.protocol.RequestDispatcherRegistry;

import javax.servlet.*;

public class ContextLoaderListener implements ServletContextListener {
    //servlet容器启动时自动调用
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();

        ApplicationContext applicationContext = createApplicationContext(servletContext.getInitParameter("configuration"),WebUtils.createPropertyResolver());
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("dispatcherServlet",dispatcherServlet);
        servletRegistration.addMapping("/");
        servletRegistration.setLoadOnStartup(0);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("servlet destroy .. ");
    }

    public ApplicationContext createApplicationContext(String configClassName,PropertyResolver propertyResolver){
        Class<?> cls = null;
        try {
            if(configClassName==null||"".equals(configClassName)){
                throw new RuntimeException("can not init ApplicationContext for missing init param:configuration");
            }
            cls = Class.forName(configClassName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("cat not load class from init param 'configuration' :"+configClassName);
        }
        return new AnnotationConfigApplicationContext(cls,propertyResolver);
    }
}
