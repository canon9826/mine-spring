package com.study.mvc.ioc;

import com.study.spring.beanDefinition.Value;
import com.study.spring.property.PropertyResolver;
import com.study.spring.yaml.ClassPathUtils;
import com.study.spring.yaml.YamlUtils;

import java.io.FileNotFoundException;
import java.io.UncheckedIOException;
import java.util.Map;
import java.util.Properties;

public class WebUtils {
    public static String CONFIG_APP_YAML = "/webApplication.yaml";
    public static String CONFIG_APP_PROPERTIES = "/webApplication.properties";

    public static PropertyResolver createPropertyResolver(){
         Properties properties = new Properties();
        try {
            Map<String, Object> yamlMap = YamlUtils.loadYamlAsPlainMap(CONFIG_APP_YAML);
            for (String key : yamlMap.keySet()) {
                Object value = yamlMap.get(key);
                if(value instanceof  String){
                    properties.put(key,(String)value);
                }
            }
        }catch (UncheckedIOException e){
            if(e.getCause() instanceof FileNotFoundException){
                ClassPathUtils.readInputStream(CONFIG_APP_PROPERTIES,input->{
                    properties.load(input);
                    return true;
                });
            }
        }
        return new PropertyResolver(properties);
    }
}
