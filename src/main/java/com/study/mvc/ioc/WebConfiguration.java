package com.study.mvc.ioc;

import com.study.spring.beanDefinition.ComponentScan;
import com.study.spring.beanDefinition.Configuration;

@Configuration
@ComponentScan
public class WebConfiguration {
}
