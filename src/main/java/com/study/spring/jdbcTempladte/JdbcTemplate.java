/**
 * @ClassName JdbcTemplate
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 22:32
 * @Version 1.0
 **/

package com.study.spring.jdbcTempladte;

import com.study.spring.jdbc.transaction.TransactionalUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JdbcTemplate {
    private DataSource dataSource;

    public JdbcTemplate(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //execute
    public <T> T execute(ConnectionCallBack<T> action) throws DataAccessException{
        //先获取当前事务连接
        Connection current = TransactionalUtils.getCurrentConnection();
        if(current!=null){
            try {
                return action.doInConnection(current);
            } catch (SQLException throwables) {
                throw new DataAccessException(throwables);
            }
        }
        //无事务,从连接池获取连接
        try(Connection newConn = dataSource.getConnection()){
            T result = action.doInConnection(newConn);
            return result;
        }catch (SQLException e){
            throw new DataAccessException(e);
        }
    }
    //PreparedStatement

    public <T> T execute(PreparedStatementCreator psc ,PreparedStatementCallback<T> action){
        //调用和实现抽象方法：ConnectionCallBack.doInConnection(Connection conn)
        return execute(new ConnectionCallBack<T>() {
            @Override
            public T doInConnection(Connection con) throws SQLException {
                try(PreparedStatement ps = psc.createPreparedStatement(con)){
                    return action.doInPreparedStatement(ps);
                }
            }
        });
//        return execute((Connection con)->{
//            try(PreparedStatement ps = psc.createPreparedStatement(con)){
//                return action.doInPreparedStatement(ps);
//            }
//        });
    }


    public int update(String sql , Object ... args) throws DataAccessException{
        //调用execute(PreparedStatementCreator psc ,PreparedStatementCallback<T> action)
        /*psc为preparedStatementCreator(String sql,Object ... args)的返回值
            preparedStatementCreator为实现抽象类PreparedStatementCreator的createPreparedStatement(Connection conn)
        action为实现抽象类PreparedStatementCallback的doInPreparedStatement(PreparedStatement ps)*/
        //->execute(ConnectionCallBack<T> action)
        /*action为实现抽象类ConnectionCallBack的doInConnection(Connection conn)*/
        return execute(
                preparedStatementCreator(sql, args),
                new PreparedStatementCallback<Integer>() {
                    @Override
                    public Integer doInPreparedStatement(PreparedStatement ps) throws SQLException {
                        return ps.executeUpdate();
                    }
                }
                /*(PreparedStatement ps)->{
                    return ps.executeUpdate();
                }*/);
    }

    private PreparedStatementCreator preparedStatementCreator(String sql,Object ... args){
        return new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sql);
                bindArgs(ps,args);
                return ps;
            }
        };
//        return (Connection con)->{
//            PreparedStatement ps = con.prepareStatement(sql);
//            bindArgs(ps,args);
//            return ps;
//        };
    }

    private void bindArgs(PreparedStatement ps ,Object ... args) throws SQLException{
        for (int i = 0; i < args.length; i++) {
            ps.setObject(i+1,args[i]);
        }
    }
}
