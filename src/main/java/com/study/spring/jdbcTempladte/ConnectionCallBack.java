package com.study.spring.jdbcTempladte;

import jakarta.annotation.Nullable;

import java.sql.Connection;
import java.sql.SQLException;

@FunctionalInterface
public interface ConnectionCallBack<T> {
    @Nullable
    T doInConnection(Connection con) throws SQLException;
}
