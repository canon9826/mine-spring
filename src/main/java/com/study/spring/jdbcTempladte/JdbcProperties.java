/**
 * @ClassName Datasource
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 21:12
 * @Version 1.0
 **/

package com.study.spring.jdbcTempladte;

import com.study.spring.beanDefinition.Configuration;
import com.study.spring.beanDefinition.Value;

@Configuration
public class JdbcProperties {

    @Value("${mine.spring.datasource.url}")
    private String url;

    @Value("${mine.spring.datasource.username}")
    private String username;

    @Value("${mine.spring.datasource.password}")
    private String password;

    @Value("${mine.spring.datasource.driver}")
    private String driver;

    @Value("${mine.spring.datasource.maximum-pool-size:20}")
    private int maximumPoolSize;

    @Value("${mine.spring.datasource.minimum-pool-size:1}")
    private int minimumPoolSize;

    @Value("${mine.spring.datasource.connection-timeout:30000}")
    private long connectionTimeout;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getMinimumPoolSize() {
        return minimumPoolSize;
    }

    public void setMinimumPoolSize(int minimumPoolSize) {
        this.minimumPoolSize = minimumPoolSize;
    }

    public long getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }
}
