package com.study.spring.jdbcTempladte;

import com.study.spring.beanDefinition.ComponentScan;
import com.study.spring.beanDefinition.Configuration;
import com.study.spring.beanDefinition.Import;

@Import(JdbcConfiguration.class)
@ComponentScan
@Configuration
public class AppConfig {
}
