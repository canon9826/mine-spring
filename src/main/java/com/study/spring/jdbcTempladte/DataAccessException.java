package com.study.spring.jdbcTempladte;

import com.study.spring.beanDefinition.exception.NestedRuntimeException;

public class DataAccessException extends NestedRuntimeException {
    public DataAccessException() {
    }

    public DataAccessException(String message) {
        super(message);
    }

    public DataAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataAccessException(Throwable cause) {
        super(cause);
    }
}
