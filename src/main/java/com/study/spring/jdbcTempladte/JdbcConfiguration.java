/**
 * @ClassName JdbcConfiguration
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/5/19 21:21
 * @Version 1.0
 **/

package com.study.spring.jdbcTempladte;

import com.study.spring.beanDefinition.Autowired;
import com.study.spring.beanDefinition.Bean;
import com.study.spring.beanDefinition.Configuration;
import com.study.spring.jdbc.transaction.DataSourceTransactionManager;
import com.study.spring.jdbc.transaction.PlatformTransactionManager;
import com.study.spring.jdbc.transaction.TransactionalBeanPostProcessor;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

@Configuration
public class JdbcConfiguration {

    @Autowired
    private JdbcProperties properties;

    @Bean(destroyMethod = "close")
    public DataSource dataSource(){
        //使用hikari连接池，配置jdbc连接参数，获取连接
        HikariConfig config = new HikariConfig();
        config.setAutoCommit(false);
        config.setJdbcUrl(properties.getUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());
        if (properties.getDriver() != null) {
            config.setDriverClassName(properties.getDriver());
        }
        config.setMaximumPoolSize(properties.getMaximumPoolSize());
        config.setMinimumIdle(properties.getMinimumPoolSize());
        config.setConnectionTimeout(properties.getConnectionTimeout());
        return new HikariDataSource(config);
    }

    //transaction
    @Bean
    JdbcTemplate jdbcTemplate(@Autowired DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    @Bean
    TransactionalBeanPostProcessor transactionalBeanPostProcessor(){
        return new TransactionalBeanPostProcessor();
    }

    @Bean
    PlatformTransactionManager platformTransactionManager(@Autowired DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }

}
