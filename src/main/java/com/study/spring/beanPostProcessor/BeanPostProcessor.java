/**
 * @InterfaceName BeanPostProcessor
 * @Decsription bean延迟处理器
 * @Author GuoJunJie
 * @Date 2024/4/20 11:18
 * @Version 1.0
 **/

package com.study.spring.beanPostProcessor;

public interface BeanPostProcessor {

    /**
     * Invoked after new Bean().
     * @param bean
     * @param beanName
     * @return
     */
    default Object postProcessBeforeInitialization(Object bean,String beanName){
        return bean;
    }

    /**
     * Invoked after bean.init() called.
     * @param bean
     * @param beanName
     * @return
     */
    default Object postProcessAfterInitialization(Object bean,String beanName){
        return bean;
    }

    /**
     * Invoked before bean.setXyz() called.
     * @param bean
     * @param beanName
     * @return
     */
    default  Object postProcessOnSetProperty(Object bean,String beanName){
        return bean;
    }
}
