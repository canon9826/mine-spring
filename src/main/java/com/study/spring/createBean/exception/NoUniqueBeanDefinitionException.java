/**
 * @ClassName NoUniqueBeanDefinitionException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/13 11:10
 * @Version 1.0
 **/

package com.study.spring.createBean.exception;

import com.study.spring.beanDefinition.exception.BeanDefinitionException;

public class NoUniqueBeanDefinitionException extends BeanDefinitionException {
    public NoUniqueBeanDefinitionException() {
    }

    public NoUniqueBeanDefinitionException(String message) {
        super(message);
    }
}
