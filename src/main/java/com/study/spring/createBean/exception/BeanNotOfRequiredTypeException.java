/**
 * @ClassName BeanNotOfRequiredTypeException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/13 11:09
 * @Version 1.0
 **/

package com.study.spring.createBean.exception;

import com.study.spring.beanDefinition.exception.BeanDefinitionException;

public class BeanNotOfRequiredTypeException extends BeanDefinitionException {
    public BeanNotOfRequiredTypeException() {
    }

    public BeanNotOfRequiredTypeException(String message) {
        super(message);
    }
}
