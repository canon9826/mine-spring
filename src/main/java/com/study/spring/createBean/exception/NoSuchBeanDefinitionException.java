/**
 * @ClassName NoSuchBeanDefinitionException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/13 11:08
 * @Version 1.0
 **/

package com.study.spring.createBean.exception;

import com.study.spring.beanDefinition.exception.BeanDefinitionException;

public class NoSuchBeanDefinitionException extends BeanDefinitionException {
    public NoSuchBeanDefinitionException() {
    }

    public NoSuchBeanDefinitionException(String message) {
        super(message);
    }
}
