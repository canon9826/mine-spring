/**
 * @ClassName UnsatisfaiedDependencyException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/13 10:26
 * @Version 1.0
 **/

package com.study.spring.createBean.exception;

import com.study.spring.beanDefinition.exception.BeanCreationException;

public class UnsatisfiedDependencyException extends BeanCreationException {
    public UnsatisfiedDependencyException() {
    }

    public UnsatisfiedDependencyException(String message) {
        super(message);
    }

    public UnsatisfiedDependencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsatisfiedDependencyException(Throwable cause) {
        super(cause);
    }
}
