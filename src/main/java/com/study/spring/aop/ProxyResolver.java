/**
 * @ClassName ProxyReslover
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/22 11:48
 * @Version 1.0
 **/

package com.study.spring.aop;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.scaffold.subclass.ConstructorStrategy;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.matcher.ElementMatchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;

public class ProxyResolver {
    final Logger logger = LoggerFactory.getLogger(getClass());

    final ByteBuddy byteBuddy = new ByteBuddy();

    private static ProxyResolver INSTANCE = null;

    public static ProxyResolver getInstance(){
        if(INSTANCE == null){
            INSTANCE = new ProxyResolver();
        }
        return INSTANCE;
    }

    public <T> T createProxy(T bean, InvocationHandler handler) {
        Class<?> targetClass = bean.getClass();
        logger.atDebug().log("create proxy for bean {} @{}", targetClass.getName(), Integer.toHexString(bean.hashCode()));
        Class<?> proxyClass = this.byteBuddy
                //创建新的类构造器,类型为targetClass,构造函数为父类的默认构造,且必须为public
                .subclass(targetClass, ConstructorStrategy.Default.DEFAULT_CONSTRUCTOR)
                //拦截所有公共方法
                .method(ElementMatchers.isPublic())
                .intercept(InvocationHandlerAdapter.of(
                        //方法调用代理至原始bean
                        (proxy, method, args) -> handler.invoke(bean, method, args)))
                //生成字节码
                .make()
                //加载字节码文件
                .load(targetClass.getClassLoader()).getLoaded();
        //创建proxy实例
        Object proxy;
        try {
            proxy = proxyClass.getConstructor().newInstance();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return (T) proxy;
    }

}
