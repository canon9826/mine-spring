/**
 * @EnumName Arount
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/23 19:12
 * @Version 1.0
 **/
package com.study.spring.around;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Around {

    /**
     * invocation handler bean name
     * @return
     */
    String value();
}
