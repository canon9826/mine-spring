/**
 * @ClassName AnnotationProxyBeanPostProcessor
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/23 19:14
 * @Version 1.0
 **/

package com.study.spring.around;

import com.study.spring.aop.ProxyResolver;
import com.study.spring.beanDefinition.BeanDefinition;
import com.study.spring.beanPostProcessor.BeanPostProcessor;
import com.study.spring.ioc.ApplicationContextUtils;
import com.study.spring.ioc.ConfigurableApplicationContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public abstract class AnnotationProxyBeanPostProcessor<A extends Annotation> implements BeanPostProcessor {
    Map<String,Object> originBeans = new HashMap<>();
    Class<A> annotationClass;

    public AnnotationProxyBeanPostProcessor(){
        this.annotationClass = getParameterizedType();
    }

    /**
     * 获取使用时该类对应的注解的类型
     * @return
     */
    private Class<A> getParameterizedType() {
        //返回该类的直接实体的类型
        Type type = getClass().getGenericSuperclass();
        //是否属于参数化类型
        if(!(type instanceof ParameterizedType)){
            throw new IllegalArgumentException("Class " + getClass().getName() + " does not have parameterized type.");
        }
        ParameterizedType pt = (ParameterizedType) type;
        //返回表示此类型的实际类型参数的Type对象数组。
        Type[] types = pt.getActualTypeArguments();
        if(types.length != 1){
            throw new IllegalArgumentException("Class " + getClass().getName() + " has more than 1 parameterized types.");
        }
        Type r = types[0];
        //检查参数类型与当前类型是否一致
        if(!(r instanceof Class<?>)){
            throw new IllegalArgumentException("Class " + getClass().getName() + " does not have parameterized type of class.");
        }
        return (Class<A>) r;
    }


    /**
     * 创建bean完成后调用所有延后处理器的该方法
     * @param bean
     * @param beanName
     * @return
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        Class<?> beanClass = bean.getClass();
        //around
        A annotation = beanClass.getAnnotation(annotationClass);
        if(annotation!=null){
            String handlerName;
            try {
                //获取around注解的value的内容
                handlerName = (String) annotation.annotationType().getMethod("value").invoke(annotation);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(String.format("@%s must have value() returned String type.", this.annotationClass.getSimpleName()), e);
            }
            //创建代理类
            Object proxy =createProxy(beanClass,bean,handlerName);
            originBeans.put(beanName,bean);
            return proxy;
        }
        return bean;
    }

    /**
     * 生成指定代理处理器的处理的指定bean的代理
     * @param beanClass
     * @param bean 要生成代理的bean
     * @param handlerName 代理处理器
     * @return
     */
    private Object createProxy(Class<?> beanClass, Object bean, String handlerName) {
        ConfigurableApplicationContext ctx = (ConfigurableApplicationContext) ApplicationContextUtils.getRequiredApplicationContext();
        BeanDefinition definition = ctx.findBeanDefinition(handlerName);
        if(definition == null){
            throw new RuntimeException(String.format("@%s proxy handler '%s' not found.", this.annotationClass.getSimpleName(), handlerName));
        }
        Object handlerBean = definition.getInstance();
        if (handlerBean == null) {
            handlerBean = ctx.createBeanAsEarlySingleton(definition);
        }
        if (!(handlerBean instanceof InvocationHandler)) {
            throw new RuntimeException(String.format("@%s proxy handler '%s' is not type of %s.", this.annotationClass.getSimpleName(), handlerName,
                    InvocationHandler.class.getName()));
        }
        //生成代理类并加载
        return ProxyResolver.getInstance().createProxy(bean,(InvocationHandler) handlerBean);

    }

    /**
     * 属性注入原始bean
     * @param bean
     * @param beanName
     * @return
     */
    @Override
    public Object postProcessOnSetProperty(Object bean, String beanName) {
        Object origin = this.originBeans.get(beanName);
        return origin !=null ? origin : bean;
    }
}
