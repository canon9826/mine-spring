/**
 * @ClassName BeforeInvocationHandlerAdapter
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/23 19:39
 * @Version 1.0
 **/

package com.study.spring.around;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public abstract class BeforeInvocationHandlerAdapter implements InvocationHandler {

    public abstract void before(Object proxy, Method method,Object[] args);

    @Override
    public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before(proxy, method, args);
        return method.invoke(proxy,args);
    }
}
