/**
 * @ClassName AfterInvocationHandlerAdapter
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/23 19:40
 * @Version 1.0
 **/

package com.study.spring.around;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public abstract class AfterInvocationHandlerAdapter implements InvocationHandler {

    public abstract void after(Object proxy, Method method, Object[] args);

    @Override
    public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        after(proxy, method, args);
        return method.invoke(proxy,args);
    }
}
