/**
 * @ClassName PropertyResolver
 * @Description 保存所有配置项，对外提供查询功能
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/1 9:29
 * @Version 1.0
 **/

package com.study.spring.property;



import jakarta.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.util.*;
import java.util.function.Function;

public class PropertyResolver {

    Logger logger = LoggerFactory.getLogger(getClass());

    Map<String,String> properties = new HashMap<>();

    static Map<Class<?>, Function<String,Object>> converters = new HashMap<>();
    static {
        converters.put(String.class, s -> s);
        converters.put(boolean.class, s -> Boolean.parseBoolean(s));
        converters.put(Boolean.class, s -> Boolean.valueOf(s));

        converters.put(byte.class, s -> Byte.parseByte(s));
        converters.put(Byte.class, s -> Byte.valueOf(s));

        converters.put(short.class, s -> Short.parseShort(s));
        converters.put(Short.class, s -> Short.valueOf(s));

        converters.put(int.class, s -> Integer.parseInt(s));
        converters.put(Integer.class, s -> Integer.valueOf(s));

        converters.put(long.class, s -> Long.parseLong(s));
        converters.put(Long.class, s -> Long.valueOf(s));

        converters.put(float.class, s -> Float.parseFloat(s));
        converters.put(Float.class, s -> Float.valueOf(s));

        converters.put(double.class, s -> Double.parseDouble(s));
        converters.put(Double.class, s -> Double.valueOf(s));

        converters.put(LocalDate.class, s -> LocalDate.parse(s));
        converters.put(LocalTime.class, s -> LocalTime.parse(s));
        converters.put(LocalDateTime.class, s -> LocalDateTime.parse(s));
        converters.put(ZonedDateTime.class, s -> ZonedDateTime.parse(s));
        converters.put(Duration.class, s -> Duration.parse(s));
        converters.put(ZoneId.class, s -> ZoneId.of(s));


    }

    public PropertyResolver(Properties props) {
        //存入环境变量
        this.properties.putAll(System.getenv());
        Set<String> names = props.stringPropertyNames();
        for (String name : names) {
            this.properties.put(name,props.getProperty(name));
        }
        //debug模式输出所有注册的key
        if (logger.isDebugEnabled()) {
            List<String> keys = new ArrayList<>(this.properties.keySet());
            Collections.sort(keys);
            for (String key : keys) {
                logger.debug("PropertyResolver: {} = {}", key, this.properties.get(key));
            }
        }
    }

    public boolean containsProperty(String key){
        return this.properties.containsKey(key);
    }



    @Nullable
    public String getProperty(String key) {
        //解析${key:defaultValue}
        PropertyExpr propertyExpr = parsePropertyExpr(key);
        if (propertyExpr != null) {
            if (propertyExpr.defaultValue() != null) {
                return getProperty(propertyExpr.key(), propertyExpr.defaultValue());
            } else {
                return getRequiredProperty(propertyExpr.key());
            }
        }
        String val = this.properties.get(key);
        if (val != null) {
            return parseValue(val);
        }
        return val;
    }

    /**
     * 实际获取有默认值的key
     * @param key
     * @param defaultValue
     * @return
     */
    @Nullable
    private String getProperty(String key, String defaultValue) {
        String val = getProperty(key);
        return val == null ? parseValue(defaultValue) : val;
    }

    public <T> T getProperty(String key,Class<T> targetType){
        String val = getProperty(key);
        if (val == null) {
            return null;
        }
        return convert(targetType,val);
    }


    @Nullable
    public <T> T getProperty(String key,Class<T> targetType,T defaultValue){
        String val = getProperty(key);
        if (val == null) {
            return defaultValue;
        }
        return convert(targetType,val);
    }


    /**
     * 获取程序环境中指定key的属性
     * @param key
     * @return
     */
    public String getRequiredProperty(String key) {
        String val = getProperty(key);
        if (val == null) {
            throw new IllegalArgumentException("can not get key,check it or set default value of : " + key);
        }
        return val;
    }

    /**
     * 获取程序环境中指定类型,指定key的属性
     * @param key
     * @return
     */
    public <T> T getRequiredProperty(String key,Class<T> targetType) {
        T val = getProperty(key,targetType);
        if (val == null) {
            throw new IllegalArgumentException("can not get key,check it or set default value of : " + key);
        }
        return val;
    }



    private <T> T convert(Class<T> clazz, String val) {
        //class->Function
        Function<String, Object> function = PropertyResolver.converters.get(clazz);
        if (function == null) {
            throw new IllegalArgumentException("Unsupported value type: " + clazz.getName());
        }
        return (T)function.apply(val);
    }


    private String parseValue(String val) {
        PropertyExpr propertyExpr = parsePropertyExpr(val);
        if (propertyExpr == null) {
            return val;
        }
        if (propertyExpr.defaultValue() == null) {
            return getProperty(propertyExpr.key(),propertyExpr.defaultValue());
        }
        return getRequiredProperty(propertyExpr.key());
    }





    /**
     * 解析${key:default}
     * 可支持嵌套,如:{@code ${app.title:${APP_NAME:Summer}}}
     * @param key
     * @return
     */
    public PropertyExpr parsePropertyExpr(String key) {
        if (key.startsWith("${") && key.endsWith("}")) {
            int split = key.indexOf(":");
            String keyVal = key.substring(2,split==-1?key.length()-1:split);
            if (keyVal.isEmpty()) {
                throw new IllegalArgumentException("invalid key :"+key);
            }
            if (split == -1) {
                //${key} no defaultValue
                return new PropertyExpr(keyVal, null);
            } else {
                //${key:default} have defaultValue
                return new PropertyExpr(keyVal, key.substring(split + 1, key.length() - 1));
            }
        }
        return null;
    }
}
