/**
 * @ClassName PropertyTest
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/1 11:45
 * @Version 1.0
 **/

package com.study.spring.property;

import com.study.spring.yaml.YamlUtils;

import java.util.Map;

public class PropertyTest {
    public static void main(String[] args) {
        //PropertyResolver test
        //Properties props = new Properties();
        //props.setProperty("app.title", "Summer Framework");
        //props.setProperty("app.version", "v1.0");
        //props.setProperty("jdbc.url", "jdbc:mysql://localhost:3306/simpsons");
        //props.setProperty("jdbc.username", "bart");
        //props.setProperty("jdbc.password", "51mp50n");
        //props.setProperty("jdbc.pool-size", "20");
        //props.setProperty("jdbc.auto-commit", "true");
        //props.setProperty("scheduler.started-at", "2023-03-29T21:45:01");
        //props.setProperty("scheduler.backup-at", "03:05:10");
        //props.setProperty("scheduler.cleanup", "P2DT8H21M");
        //
        //PropertyResolver resolver = new PropertyResolver(props);
        //System.out.println(resolver.getProperty("app.version"));
        //System.out.println(resolver.getProperty("${app.version}"));
        //System.out.println(resolver.getProperty("${app.versions:1.1}"));
        //System.out.println(resolver.getProperty("${scheduler.started-at}", LocalDateTime.class));
        //System.out.println(resolver.getProperty("${scheduler.started-ae:2024-04-02T20:19:33}", LocalDateTime.class));

        //yaml test
        final Map<String, Object> config = YamlUtils.loadYamlAsPlainMap("/Application.yml");
        for (String key : config.keySet()) {
            final Object val = config.get(key);
            System.out.println(String.format("key:%s\tvalue.class:%s",key,val.getClass()));
        }
        System.out.println(config.get("mine.spring.data.jdbc.url"));
    }
}
