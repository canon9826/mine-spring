/**
 * @ClassName PropertyExpr
 * @Description 用于存储解析后的${abc.xyz:defaultValue}这样的key
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/1 9:47
 * @Version 1.0
 **/

package com.study.spring.property;

import java.util.Objects;

public final class PropertyExpr {
    final String key;
    final String defaultValue;

    public PropertyExpr(String key, String defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public String key() {
        return key;
    }

    public String defaultValue() {
        return defaultValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyExpr)) return false;
        PropertyExpr that = (PropertyExpr) o;
        return Objects.equals(key, that.key) && Objects.equals(defaultValue, that.defaultValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, defaultValue);
    }

    @Override
    public String toString() {
        return "PropertyExpr{" +
                "key='" + key + '\'' +
                ", defaultValue='" + defaultValue + '\'' +
                '}';
    }
}
