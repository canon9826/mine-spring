/**
 * @InterfaceName ConfigurableApplicationContext
 * @Description 系统测接口
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/22 9:33
 * @Version 1.0
 **/

package com.study.spring.ioc;

import com.study.spring.beanDefinition.BeanDefinition;
import jakarta.annotation.Nullable;

import java.util.List;

public interface ConfigurableApplicationContext extends ApplicationContext{

    List<BeanDefinition> findBeanDefinitions(Class<?> type);

    @Nullable
    BeanDefinition findBeanDefinition(Class<?> type);

    @Nullable
    BeanDefinition findBeanDefinition(String name);

    @Nullable
    BeanDefinition findBeanDefinition(String name,Class<?> requiredType);

    Object createBeanAsEarlySingleton(BeanDefinition def);
}
