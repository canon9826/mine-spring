/**
 * @ClassName ApplicationUtils
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/21 19:16
 * @Version 1.0
 **/

package com.study.spring.ioc;


import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.util.Objects;

public class ApplicationContextUtils {
    private static ApplicationContext applicationContext = null;

    public static void setApplicationContext(ApplicationContext ctx) {
        applicationContext = ctx;
    }

    //region unUse

    @Nonnull
    public static ApplicationContext getRequiredApplicationContext(){
        return Objects.requireNonNull(getApplicationContext(),"ApplicationContext is not set");
    }

    @Nullable
    private static ApplicationContext getApplicationContext() {
        return applicationContext;
    }


    //endregion
}
