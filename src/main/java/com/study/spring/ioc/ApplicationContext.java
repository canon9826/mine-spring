/**
 * @InterfaceName ConfigurationApplicationContext
 * @Decsription 用户测接口
 * @Author GuoJunJie
 * @Date 2024/4/21 19:09
 * @Version 1.0
 **/

package com.study.spring.ioc;

import java.util.List;

public interface ApplicationContext extends AutoCloseable{

    boolean containsBean(String name);

    <T> T getBean(String name);

    <T> T getBean(String name,Class<T> requiredType);

    <T> T getBean(Class<T> requiredType);

    <T> List<T> getBeans(Class<T> requiredType);


    void close();

}
