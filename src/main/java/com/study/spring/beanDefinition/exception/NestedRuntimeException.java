/**
 * @ClassName NestedRuntimeException
 * @Decsription 嵌套运行时异常
 * @Author GuoJunJie
 * @Date 2024/4/9 20:28
 * @Version 1.0
 **/

package com.study.spring.beanDefinition.exception;

public class NestedRuntimeException extends RuntimeException{

    public NestedRuntimeException() {
    }

    public NestedRuntimeException(String message) {
        super(message);
    }

    public NestedRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NestedRuntimeException(Throwable cause) {
        super(cause);
    }
}
