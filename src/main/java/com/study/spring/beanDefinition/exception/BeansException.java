/**
 * @ClassName BeansException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/9 20:27
 * @Version 1.0
 **/

package com.study.spring.beanDefinition.exception;

public class BeansException extends NestedRuntimeException{
    public BeansException() {
    }

    public BeansException(String message) {
        super(message);
    }

    public BeansException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeansException(Throwable cause) {
        super(cause);
    }
}
