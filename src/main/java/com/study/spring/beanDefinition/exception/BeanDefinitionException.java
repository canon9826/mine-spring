/**
 * @ClassName BeanDefinitionException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/9 20:27
 * @Version 1.0
 **/

package com.study.spring.beanDefinition.exception;

public class BeanDefinitionException extends BeansException{

    public BeanDefinitionException() {
    }

    public BeanDefinitionException(String message) {
        super(message);
    }

    public BeanDefinitionException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanDefinitionException(Throwable cause) {
        super(cause);
    }
}
