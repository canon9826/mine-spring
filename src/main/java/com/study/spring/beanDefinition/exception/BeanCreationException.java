/**
 * @ClassName BeanCreationException
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/9 20:46
 * @Version 1.0
 **/

package com.study.spring.beanDefinition.exception;

public class BeanCreationException extends BeansException{

    public BeanCreationException() {
    }

    public BeanCreationException(String message) {
        super(message);
    }

    public BeanCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanCreationException(Throwable cause) {
        super(cause);
    }
}
