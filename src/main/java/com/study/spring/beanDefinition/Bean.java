/**
 * @AnnotationName Bean
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/7 9:40
 * @Version 1.0
 **/
package com.study.spring.beanDefinition;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Bean {

    /**
     * Bean name . default is method name
     * @return
     */
    String value() default "";

    String initMethod() default "";

    String destroyMethod() default "";

}
