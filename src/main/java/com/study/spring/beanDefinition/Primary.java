/**
 * @EnumName Primary
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/7 20:11
 * @Version 1.0
 **/
package com.study.spring.beanDefinition;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Primary {
}
