/**
 * @AnnotationName ComponentScan
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/7 9:51
 * @Version 1.0
 **/
package com.study.spring.beanDefinition;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ComponentScan {

    /**
     * package name to scan . default is current package
     * @return
     */
    String[] value() default {};
}
