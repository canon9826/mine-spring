/**
 * @EnumName Import
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/7 20:41
 * @Version 1.0
 **/
package com.study.spring.beanDefinition;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Import {

    Class<?>[] value();
}
