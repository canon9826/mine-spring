/**
 * @AnnotationName Value
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/7 9:55
 * @Version 1.0
 **/
package com.study.spring.beanDefinition;

import java.lang.annotation.*;

@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Value {

    String value();
}
