/**
 * @ClassName ClassUtils
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/7 11:17
 * @Version 1.0
 **/

package com.study.spring.beanDefinition;

import com.study.spring.beanDefinition.exception.BeanDefinitionException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClassUtils {
    /**
     * 遍历查找Annotation
     * @param target
     * @param annotationClass
     * @param <A>
     * @return
     */
    public static  <A extends Annotation> A findAnnotation(Class<?> target, Class<A> annotationClass) {
        //在当前类的注解上查找目标注解(本级)
        A targetAnnotation = target.getAnnotation(annotationClass);
        //遍历当前类上的所有注解
        for (Annotation annotation : target.getAnnotations()) {
            Class<? extends Annotation> annotationType = annotation.annotationType();
            //不是标准注解则递归遍历
            if (!annotationType.getPackage().getName().equals("java.lang.annotation")) {
                A found = findAnnotation(annotationType, annotationClass);
                if(found!=null){
                    //目标注解重复
                    if(targetAnnotation!=null){
                        throw new BeanDefinitionException("Duplicate @" + annotationClass.getSimpleName() + " found on class " + target.getSimpleName());
                    }
                    targetAnnotation = found;
                }
            }
        }
        return targetAnnotation;
    }

    /**
     * 通过方法获取bean name
     * 查找方法上的bean注解,赋值bean注解的value
     * 如果value为空,则取方法名作为bean name
     * @param method
     * @return
     */
    public static String getBeanName(Method method) {
        //获取方法的上的bean注解
        Bean bean = method.getAnnotation(Bean.class);
        //获取bean注解的value
        String name = bean.value();
        //如果value为空,则取方法的名称
        if (name.isEmpty()) {
            name = method.getName();
        }
        return name;
    }

    /**
     * 通过类获取bean的名称
     * 查找类上的component注解,赋值component注解的value
     * 如果value为空,则取类名的小写驼峰作为bean name
     * @param clazz
     * @return
     */
    public static String getBeanName(Class<?> clazz){
        String name = "";
        //获取当前类上的component注解
        Component component = clazz.getAnnotation(Component.class);
        //找到了component注解,则直接赋值component注解的value
        if (component!=null) {
            name = component.value();
        }else{
            //遍历查找当前类上是否存在注解包含@component
            for (Annotation annotation : clazz.getAnnotations()) {
                //如果有
                if (findAnnotation(annotation.annotationType(), Component.class)!=null) {
                    try {
                        //赋值包含@Component的注解的value（传递到@component注解的值）
                        name = (String)annotation.annotationType().getMethod("value").invoke(annotation);
                    } catch (ReflectiveOperationException e) {
                        throw new BeanDefinitionException("Cannot get annotation value.", e);
                    }
                }
            }
        }
        //bean的名称没有被指定则取类名的小写驼峰形式作为bean name
        if(name.isEmpty()){
            name = clazz.getSimpleName();
            name = Character.toLowerCase(name.charAt(0))+name.substring(1);
        }
        return name;
    }

    /**
     * 查找类中是否有被目标注解修饰的没有参数的方法
     * @param clazz
     * @param annotationClazz
     * @return
     */
    public static Method findAnnotationMethod(Class<?> clazz, Class<? extends Annotation> annotationClazz) {
        //查找当前类的所有方法,是否有被目标注解所注释的方法
        List<Method> methodList = Arrays.stream(clazz.getDeclaredMethods()).filter(m -> m.isAnnotationPresent(annotationClazz)).map(m -> {
            //如果方法有参数
            if (m.getParameterCount() != 0) {
                throw new BeanDefinitionException(String.format("Method '%s' with @%s must not have argument: %s", m.getName(), annotationClazz.getSimpleName(), clazz.getName()));
            }
            return m;
        }).collect(Collectors.toList());
        if (methodList.isEmpty()) {
            return null;
        }
        if (methodList.size()==1) {
            return methodList.get(0);
        }
        throw new BeanDefinitionException(String.format("Multiple methods with @%s found in class: %s", annotationClazz.getSimpleName(), clazz.getName()));
    }

    //region createBean

    public static <A extends Annotation> A getAnnotation(Annotation[] annotations,Class<A> annotationClazz){
        for (Annotation annotation : annotations) {
            if (annotationClazz.isInstance(annotation)) {
                return (A)annotation;
            }
        }
        return null;
    }

    //endregion

    //region initBean

    public static Method getNamedMethod(Class<?> clazz,String methodName){
        try {
            return clazz.getDeclaredMethod(methodName);
        } catch (NoSuchMethodException e) {
            throw new BeanDefinitionException(String.format("Method '%s' not found in class: %s", methodName, clazz.getName()));
        }
    }
    //endregion
}
