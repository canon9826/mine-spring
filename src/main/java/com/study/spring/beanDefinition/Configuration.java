/**
 * @AnnotationName Configuration
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/7 9:52
 * @Version 1.0
 **/
package com.study.spring.beanDefinition;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Configuration {

    /**
     * bean name . default is simple class name with first-class-lowercase
     * @return
     */
    String value() default "";
}
