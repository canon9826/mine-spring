/**
 * @ClassName AnnotationConfigApplicationContext
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/7 11:05
 * @Version 1.0
 **/

package com.study.spring.beanDefinition;


import com.study.spring.beanPostProcessor.BeanPostProcessor;
import com.study.spring.createBean.exception.BeanNotOfRequiredTypeException;
import com.study.spring.createBean.exception.NoSuchBeanDefinitionException;
import com.study.spring.createBean.exception.NoUniqueBeanDefinitionException;
import com.study.spring.createBean.exception.UnsatisfiedDependencyException;
import com.study.spring.beanDefinition.exception.BeanCreationException;
import com.study.spring.beanDefinition.exception.BeanDefinitionException;
import com.study.spring.ioc.ApplicationContextUtils;
import com.study.spring.ioc.ConfigurableApplicationContext;
import com.study.spring.property.PropertyResolver;
import com.study.spring.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

public class AnnotationConfigApplicationContext implements ConfigurableApplicationContext {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final PropertyResolver propertyResolver;
    protected final Map<String, BeanDefinition> beans;

    private Set<String> creatingBeanNames;

    private List<BeanPostProcessor> beanPostProcessors = new ArrayList<>();

    public AnnotationConfigApplicationContext(Class<?> configClass, PropertyResolver propertyResolver) {
        ApplicationContextUtils.setApplicationContext(this);

        this.propertyResolver = propertyResolver;
        //扫描获取所有Bean的Class类型,去重后获得包名的集合
        Set<String> beanClassNames = scanForClassNames(configClass);
        //创建Bean的定义
        this.beans = createBeanDefinitions(beanClassNames);

        //创建BeanName检测循环依赖
        this.creatingBeanNames = new HashSet<>();

        //筛选出配置类先创建bean
        this.beans.values().stream()
                .filter(this::isConfigurationDefinition)
                .forEach(this::createBeanAsEarlySingleton);

        //筛选出有bean延时处理器的先创建bean,configuration类不会执行延时处理器初始化前方法
        List<BeanPostProcessor> processors = this.beans.values().stream()
                .filter(this::isBeanPostProcessorDefinition)
                .sorted()
                .map(def -> (BeanPostProcessor) createBeanAsEarlySingleton((def)))
                .collect(Collectors.toList());

        this.beanPostProcessors.addAll(processors);
        //创建普通Bean
        createNormalBeans();

        //注入bean
        this.beans.values().forEach(this::injectBean);
        //初始化bean
        this.beans.values().forEach(this::initBean);

        this.beans.values().stream().sorted().forEach(def -> {
            logger.info("bean initialized:{}", def);
        });
    }


    //region beanDefinition

    /**
     * Do component scan and return class names.
     *
     * @param configClass
     * @return
     */
    protected Set<String> scanForClassNames(Class<?> configClass) {
        //获取ComponentScan注解
        ComponentScan scan = ClassUtils.findAnnotation(configClass, ComponentScan.class);
        //没有找到componentScan,则遍历配置文件的包,否则注解指定的包
        String[] scanPackages =
                scan == null || scan.value().length == 0
                        ? new String[]{configClass.getPackage().getName()}
                        : scan.value();
        logger.info("component scan in packages: {}", Arrays.toString(scanPackages));

        Set<String> classNameSet = new HashSet<>();
        //遍历componentScan指定的所有包
        for (String pkg : scanPackages) {
            logger.info("scan package: {}", pkg);
            //解析当前路径
            ResourceResolver resourceResolver = new ResourceResolver(pkg);
            //扫描路径下所有class文件,将路径替换成包名,放入不重复集合
            List<String> classList = resourceResolver.scan(res -> {
                String name = res.name();
                if (name.endsWith(".class")) {
                    return name.substring(0, name.length() - 6).replace("/", ".").replace("\\", ".");
                }
                return null;
            });
            classNameSet.addAll(classList);

        }
        //查找@Import(xyz.class)
        Import importConfig = configClass.getAnnotation(Import.class);
        //如果找到了import注解,则遍历注解中包含的类
        if (importConfig != null) {
            for (Class<?> importConfigClazz : importConfig.value()) {
                //如果需要导入的配置类没有被扫描,则加入集合
                String importConfigClazzName = importConfigClazz.getName();
                if (classNameSet.contains(importConfigClazzName)) {
                    logger.warn("ignore import: " + importConfigClazzName + " for it is already been scanned.");
                } else {
                    logger.info("class found by import: {}", importConfigClazzName);
                    classNameSet.add(importConfigClazzName);
                }
            }
        }
        return classNameSet;
    }

    /**
     * 根据扫描的ClassName创建BeanDefinition
     *
     * @param classNameSet
     * @return
     */
    private Map<String, BeanDefinition> createBeanDefinitions(Set<String> classNameSet) {
        Map<String, BeanDefinition> defMap = new HashMap<>();
        for (String className : classNameSet) {
            Class<?> clazz = null;
            try {
                //反射获取需要创建bean的类
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                throw new BeanCreationException(e);
            }
            //无需创建的类型
            if (clazz.isAnnotation() || clazz.isEnum() || clazz.isInterface() /*jdk11:||clazz.isRecord()*/) {
                continue;
            }
            //查看当前类上是否包含component注解(递归)
            Component component = ClassUtils.findAnnotation(clazz, Component.class);
            if (component != null) {
                //获取当前类的修饰符
                int modifiers = clazz.getModifiers();
                logger.info("found component: {}", clazz.getName());
                //如果是抽象类
                if (Modifier.isAbstract(modifiers)) {
                    throw new BeanDefinitionException("@Component class " + clazz.getName() + " must not be abstract.");
                }
                //如果是私有类
                if (Modifier.isPrivate(modifiers)) {
                    throw new BeanDefinitionException("@Component class " + clazz.getName() + " must not be private.");
                }
                //通过类获取bean name
                String beanName = ClassUtils.getBeanName(clazz);
                //基于当前类创建一个BeanDefinition
                BeanDefinition def = new BeanDefinition(beanName, clazz,
                        getSuitableConstructor(clazz), getOrder(clazz),
                        //当前类是否被primary修饰,true/false
                        clazz.isAnnotationPresent(Primary.class),
                        null, null,
                        ClassUtils.findAnnotationMethod(clazz, PostConstruct.class),
                        ClassUtils.findAnnotationMethod(clazz, PreDestroy.class));
                //将创建的BeanDefinition放入不重复集合
                addBeanDefinitions(defMap, def);
                logger.info("define bean: {}", def);
                //查找当前类是否存在configuration注解(递归)
                Configuration configuration = ClassUtils.findAnnotation(clazz, Configuration.class);
                if (configuration != null) {
                    scanFactoryMethods(beanName, clazz, defMap);
                }
            }
        }
        return defMap;
    }

    /**
     * 获取当前类的合适的构造方法
     *
     * @param clazz
     * @return
     */
    public Constructor<?> getSuitableConstructor(Class<?> clazz) {
        //获取类的全部public构造方法
        Constructor<?>[] constructors = clazz.getConstructors();
        if (constructors.length == 0) {
            //获取类的全部构造方法
            constructors = clazz.getDeclaredConstructors();
            if (constructors.length != 1) {
                throw new BeanDefinitionException("More than one constructor found in class " + clazz.getName() + ".");
            }
        }
        if (constructors.length != 1) {
            throw new BeanDefinitionException("More than one public constructor found in class " + clazz.getName() + ".");
        }
        //todo spring可以指定多个构造方法时取哪一个
        return constructors[0];
    }

    /**
     * 获取当前类的加载顺序
     *
     * @param clazz
     * @return
     */
    public int getOrder(Class<?> clazz) {
        //查看当前类上是否存在order注解指定加载顺序,有则返回order指定值,否则返回int的最大值
        Order order = clazz.getAnnotation(Order.class);
        return order == null ? Integer.MAX_VALUE : order.value();
    }

    /**
     * 获取当前方法的加载顺序
     *
     * @param method
     * @return
     */
    public int getOrder(Method method) {
        Order order = method.getAnnotation(Order.class);
        return order == null ? Integer.MAX_VALUE : order.value();
    }

    /**
     * 将创建的BeanDefinition放入不重复集合
     *
     * @param defMap
     * @param beanDefinition
     */
    public void addBeanDefinitions(Map<String, BeanDefinition> defMap, BeanDefinition beanDefinition) {
        //判断是否存在重复key
        if (defMap.put(beanDefinition.getName(), beanDefinition) != null) {
            throw new BeanDefinitionException("Duplicate bean name: " + beanDefinition.getName());
        }
    }

    /**
     * 带有@Configuration注解的Class，视为Bean的工厂
     *
     * @param factoryBeanName
     * @param clazz
     * @param defMap
     */
    public void scanFactoryMethods(String factoryBeanName, Class<?> clazz, Map<String, BeanDefinition> defMap) {
        //递归当前类的所有方法
        for (Method method : clazz.getDeclaredMethods()) {
            //查看方法是否被@bean修饰
            Bean bean = method.getAnnotation(Bean.class);
            if (bean != null) {
                //获取当前方法的修饰符
                int modifiers = method.getModifiers();
                //抽象方法
                if (Modifier.isAbstract(modifiers)) {
                    throw new BeanDefinitionException("@Bean method " + clazz.getName() + "." + method.getName() + " must not be abstract.");
                }
                //常量方法
                if (Modifier.isFinal(modifiers)) {
                    throw new BeanDefinitionException("@Bean method " + clazz.getName() + "." + method.getName() + " must not be final.");
                }
                //私有方法
                if (Modifier.isPrivate(modifiers)) {
                    throw new BeanDefinitionException("@Bean method " + clazz.getName() + "." + method.getName() + " must not be private.");
                }
                //获取方法的返回类型
                Class<?> beanClass = method.getReturnType();
                //返回基本类型
                if (beanClass.isPrimitive()) {
                    throw new BeanDefinitionException("@Bean method " + clazz.getName() + "." + method.getName() + " must not return primitive type.");
                }
                //返回void
                if (beanClass == void.class || beanClass == Void.class) {
                    throw new BeanDefinitionException("@Bean method " + clazz.getName() + "." + method.getName() + " must not return void.");
                }
                //基于@Bean修饰的方法创建BeanDefinition
                BeanDefinition def = new BeanDefinition(ClassUtils.getBeanName(method), beanClass,
                        factoryBeanName,
                        method,
                        getOrder(method),
                        method.isAnnotationPresent(Primary.class),
                        bean.initMethod().isEmpty() ? null : bean.initMethod(),
                        bean.destroyMethod().isEmpty() ? null : bean.destroyMethod(),
                        null, null);
                addBeanDefinitions(defMap, def);
                logger.info("define bean: {}", def);
            }
        }
    }


    //endregion

    //region createBean

    @Override
    public Object createBeanAsEarlySingleton(BeanDefinition def) {
        logger.info("Try create bean '{}' as early singleton: {}", def.getName(), def.getBeanClass().getName());
        //判断bean是否被创建
        if (!this.creatingBeanNames.add(def.getName())) {
            throw new UnsatisfiedDependencyException(String.format("Circular dependency detected when create bean '%s'", def.getName()));
        }

        Executable executable = null;
        //获取BeanDefinition的工厂方法/构造方法
        executable = def.getFactoryName() != null ? def.getFactoryMethod() : def.getConstructor();

        Parameter[] parameters = executable.getParameters();
        Annotation[][] parameterAnnotations = executable.getParameterAnnotations();
        Object[] args = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            Annotation[] parameterAnnotation = parameterAnnotations[i];
            Value value = ClassUtils.getAnnotation(parameterAnnotation, Value.class);
            Autowired autowired = ClassUtils.getAnnotation(parameterAnnotation, Autowired.class);

            // @Configuration类型的Bean是工厂，不允许使用@Autowired创建
            boolean isConfiguration = isConfigurationDefinition(def);
            if (isConfiguration && autowired != null) {
                throw new BeanCreationException(String.format("Cannot specify @Autowired when create @Configuration bean '%s': %s.", def.getName(), def.getBeanClass().getName()));
            }

            //BeanPostProcessor不能依赖其他Bean，不允许使用@Autowired创建
            boolean isBeanPostProcessor = isBeanPostProcessorDefinition(def);
            if (isBeanPostProcessor && autowired != null) {
                throw new BeanCreationException(String.format("Cannot specify @Autowired when create BeanPostProcessor '%s': %s.", def.getName(), def.getBeanClass().getName()));
            }

            // 参数需要@Value或@Autowired两者之一
            if (value != null && autowired != null) {
                throw new BeanCreationException(
                        String.format("Cannot specify both @Autowired and @Value when create bean '%s': %s.", def.getName(), def.getBeanClass().getName()));
            }
            if (value == null && autowired == null) {
                throw new BeanCreationException(
                        String.format("Must specify @Autowired or @Value when create bean '%s': %s.", def.getName(), def.getBeanClass().getName()));
            }


            Class<?> type = parameter.getType();
            if (value != null) {
                //用@Value创建参数
                args[i] = this.propertyResolver.getRequiredProperty(value.value(), type);
            } else {
                //用@Autowired创建参数
                String name = autowired.name();
                boolean required = autowired.value();
                //获取参数对应的BeanDefinition,即创建Bean依赖的BeanDefinition
                BeanDefinition dependsOnDef = name.isEmpty() ? findBeanDefinition(type) : findBeanDefinition(name, type);
                if (required && dependsOnDef == null) {
                    throw new BeanCreationException(String.format("Missing autowired bean with type '%s' when create bean '%s': %s.", type.getName(),
                            def.getName(), def.getBeanClass().getName()));
                }
                if (dependsOnDef != null) {
                    Object autowiredInstance = dependsOnDef.getInstance();
                    //参数创建了BeanDefinition,但还没有创建Bean,且这个参数不是@configuration工厂
                    if (autowiredInstance == null && !isConfiguration) {
                        autowiredInstance = createBeanAsEarlySingleton(dependsOnDef);
                    }
                    args[i] = autowiredInstance;
                } else {
                    args[i] = null;
                }
            }
        }

        //创建Bean实例
        Object instance = null;
        if (def.getFactoryName() != null) {
            //获取工厂bean
            Object configInstance = getBean(def.getFactoryName());
            try {
                //执行工厂方法创建Bean
                instance = def.getFactoryMethod().invoke(configInstance, args);
            } catch (Exception e) {
                throw new BeanCreationException(String.format("Exception when create bean '%s': %s", def.getName(), def.getBeanClass().getName()), e);
            }
        } else {
            //执行构造方法创建Bean
            try {
                instance = def.getConstructor().newInstance(args);
            } catch (Exception e) {
                throw new BeanCreationException(String.format("Exception when create bean '%s': %s", def.getName(), def.getBeanClass().getName()), e);
            }
        }
        def.setInstance(instance);

        //bean创建完成后,执行系统加载了的bean延迟处理器(BeanPostProcessor),并执行bean初始化前方法
        for (BeanPostProcessor processor : beanPostProcessors) {
            Object processed = processor.postProcessBeforeInitialization(def.getInstance(), def.getName());
            if (processed == null) {
                throw new BeanCreationException(String.format("PostBeanProcessor returns null when process bean '%s' by %s", def.getName(), processor));
            }
            //如果返回的是新的对象则替换原bean
            if (def.getInstance() != processed) {
                logger.info("Bean '{}' was replaced by post processor {}.", def.getName(), processor.getClass().getName());
                def.setInstance(processed);
            }
        }
        return def.getInstance();
    }

    /**
     * 创建所有普通Bean(非Configuration)
     */
    private void createNormalBeans() {
        //筛选没有被创建的Bean
        List<BeanDefinition> definitionList = this.beans.values().stream()
                .filter(def -> Objects.isNull(def.getInstance()))
                .sorted()
                .collect(Collectors.toList());
        definitionList.forEach(def -> {
            //再次确认bean没有被创建
            if (def.getInstance() == null) {
                //创建Bean
                createBeanAsEarlySingleton(def);
            }
        });

    }

    /**
     * 获取名称对应的Bean
     *
     * @param name
     * @param <T>
     * @return
     */
    @Override
    public <T> T getBean(String name) {
        BeanDefinition definition = this.beans.get(name);
        if (definition == null) {
            throw new NoSuchBeanDefinitionException(String.format("No bean defined with name '%s'.", name));
        }
        return (T) definition.getRequiredInstance();
    }

    @Override
    public <T> T getBean(Class<T> requiredType) {
        BeanDefinition beanDefinition = findBeanDefinition(requiredType);
        if (beanDefinition == null) {
            throw new NoSuchBeanDefinitionException(String.format("No bean defined with type '%s'.", requiredType));
        }
        return (T) beanDefinition.getRequiredInstance();
    }

    @Override
    public <T> List<T> getBeans(Class<T> requiredType) {
        List<BeanDefinition> definitionList = findBeanDefinitions(requiredType);
        if (definitionList.isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<T> list = new ArrayList<>(definitionList.size());
        for (BeanDefinition beanDefinition : definitionList) {
            list.add((T) beanDefinition.getRequiredInstance());
        }
        return list;
    }


    /**
     * 判断BeanDefinition的类上是否存在configuration注解
     *
     * @param def
     * @return
     */
    public boolean isConfigurationDefinition(BeanDefinition def) {
        return ClassUtils.findAnnotation(def.getBeanClass(), Configuration.class) != null;
    }

    /**
     * 根据名称获取对应的BeanDefinition
     *
     * @param name
     * @return
     */
    @Override
    public BeanDefinition findBeanDefinition(String name) {
        return this.beans.get(name);
    }

    /**
     * 根据名称和返回类型获取对应的BeanDefinition
     *
     * @param name
     * @param requiredType
     * @return
     */
    @Override
    public BeanDefinition findBeanDefinition(String name, Class<?> requiredType) {
        BeanDefinition def = findBeanDefinition(name);
        if (def == null) {
            return null;
        }
        //判断获取的BeanDefinition和需要的返回类型是否一致
        if (!requiredType.isAssignableFrom(def.getBeanClass())) {
            throw new BeanNotOfRequiredTypeException(String.format("Autowire required type '%s' but bean '%s' has actual type '%s'.", requiredType.getName(),
                    name, def.getBeanClass().getName()));
        }
        return def;
    }

    /**
     * 获取参数类型对应的BeanDefinition
     *
     * @param type
     * @return
     */
    @Override
    public BeanDefinition findBeanDefinition(Class<?> type) {
        List<BeanDefinition> defList = findBeanDefinitions(type);
        if (defList.isEmpty()) {
            return null;
        }
        if (defList.size() == 1) {
            return defList.get(0);
        }
        //多个BeanDefinition则判断是否有primary注解
        List<BeanDefinition> primaryDefList = defList.stream()
                .filter(BeanDefinition::isPrimary)
                .collect(Collectors.toList());
        if (primaryDefList.size() == 1) {
            return primaryDefList.get(0);
        }
        if (primaryDefList.isEmpty()) {
            throw new NoUniqueBeanDefinitionException(String.format("Multiple bean with type '%s' found, but no @Primary specified.", type.getName()));
        } else {
            throw new NoUniqueBeanDefinitionException(String.format("Multiple bean with type '%s' found, and multiple @Primary specified.", type.getName()));
        }
    }

    /**
     * 获取该class类型的所有创建的BeanDefinition
     *
     * @param type
     * @return
     */
    @Override
    public List<BeanDefinition> findBeanDefinitions(Class<?> type) {
        return this.beans.values().stream()
                .filter(def -> type.isAssignableFrom(def.getBeanClass()))
                .sorted().collect(Collectors.toList());
    }


    //endregion

    //region beanPostProcessor

    /**
     * BeanDefinition是否可以转换为BeanPostProcessor
     *
     * @param def
     * @return
     */
    private boolean isBeanPostProcessorDefinition(BeanDefinition def) {
        return BeanPostProcessor.class.isAssignableFrom(def.getBeanClass());
    }

    /**
     * 获取原始bean
     * @param def
     * @return
     */
    private Object getProxiedInstance(BeanDefinition def) {
        //获取BeanDefinition存储的bean实例
        Object beanInstance = def.getInstance();
        // 如果Proxy改变了原始Bean，又希望注入到原始Bean，则由BeanPostProcessor指定原始Bean
        ArrayList<BeanPostProcessor> reversedBeanPostProcessors = new ArrayList<>(this.beanPostProcessors);
        Collections.reverse(reversedBeanPostProcessors);
        for (BeanPostProcessor beanPostProcessor : reversedBeanPostProcessors) {
            Object restoredInstance = beanPostProcessor.postProcessOnSetProperty(beanInstance, def.getName());
            if (restoredInstance != beanInstance) {
                beanInstance = restoredInstance;
            }
        }
        return beanInstance;
    }

    //endregion

    //region initBean

    /**
     * 注入bean
     *
     * @param def
     */
    private void injectBean(BeanDefinition def) {
        Object beanInstance = getProxiedInstance(def);
        try {
            //注入属性
            injectProperties(def, def.getBeanClass(), beanInstance);
        } catch (Exception e) {
            throw new BeanCreationException(e);
        }
    }


    /**
     * 初始化bean
     *
     * @param def
     */
    private void initBean(BeanDefinition def) {
        Object beanInstance = getProxiedInstance(def);
        callMethod(beanInstance, def.getInitMethod(), def.getInitMethodName());
        beanPostProcessors.forEach(beanPostProcessor -> {
            //执行延时器初始化后方法
            Object processedInstance = beanPostProcessor.postProcessAfterInitialization(def.getInstance(), def.getName());
            if(processedInstance != def.getInstance()){
                logger.info("BeanPostProcessor {} return different bean from {} to {}.", beanPostProcessor.getClass().getSimpleName(),
                        def.getInstance().getClass().getName(), processedInstance.getClass().getName());
                def.setInstance(processedInstance);
            }
        });
    }


    /**
     * 注入属性
     *
     * @param def
     * @param clazz
     * @param instance
     * @throws ReflectiveOperationException
     */
    private void injectProperties(BeanDefinition def, Class<?> clazz, Object instance) throws ReflectiveOperationException {
        //遍历类的所有字段
        for (Field field : clazz.getDeclaredFields()) {
            tryInjectProperties(def, clazz, instance, field);
        }
        //遍历类所有方法
        for (Method method : clazz.getDeclaredMethods()) {
            tryInjectProperties(def, clazz, instance, method);
        }
        //如果有父类,注入父类的所有属性
        Class<?> superClazz = clazz.getSuperclass();
        if (superClazz != null) {
            injectProperties(def, superClazz, instance);
        }
    }

    /**
     * 尝试注入属性
     *
     * @param def
     * @param beanClass
     * @param instance
     * @param accObj
     * @throws ReflectiveOperationException
     */
    private void tryInjectProperties(BeanDefinition def, Class<?> beanClass, Object instance, AccessibleObject accObj) throws ReflectiveOperationException {
        Value value = accObj.getAnnotation(Value.class);
        Autowired autowired = accObj.getAnnotation(Autowired.class);
        if (value == null && autowired == null) {
            return;
        }

        Field field = null;
        Method method = null;
        if (accObj instanceof Field) {
            Field f = (Field) accObj;
            checkFieldOrMethod(f);
            f.setAccessible(true);
            field = f;
        }
        if (accObj instanceof Method) {
            Method m = (Method) accObj;
            checkFieldOrMethod(m);
            if (m.getParameters().length != 1) {
                throw new BeanDefinitionException(String.format("Cannot inject a non-setter method %s for bean '%s': %s", m.getName(), def.getName(), def.getBeanClass().getName()));
            }
            m.setAccessible(true);
            method = m;
        }

        String accObjName = field != null ? field.getName() : method.getName();
        Class<?> accObjType = field != null ? field.getType() : method.getParameterTypes()[0];
        if (value != null && autowired != null) {
            throw new BeanCreationException(String.format("Cannot specify both @Autowired and @Value when inject %s.%s for bean '%s': %s",
                    beanClass.getSimpleName(), accObjName, def.getName(), def.getBeanClass().getName()));
        }
        //用@Value获取的属性值赋值
        if (value != null) {
            Object propertyValue = this.propertyResolver.getRequiredProperty(value.value(), accObjType);
            if (field != null) {
                logger.info("Field injection: {}.{} = {}", def.getBeanClass().getName(), accObjName, propertyValue);
                field.set(instance, propertyValue);
            }
            if (method != null) {
                logger.info("Method injection: {}.{} ({})", def.getBeanClass().getName(), accObjName, propertyValue);
                method.invoke(instance, propertyValue);
            }
        }
        //用@Autowired注入的已创建的bean赋值
        if (autowired != null) {
            String name = autowired.name();
            boolean required = autowired.value();
            Object depends = name.isEmpty() ? findBean(accObjType) : findBean(name, accObjType);
            if (required && depends == null) {
                throw new UnsatisfiedDependencyException(String.format("Dependency bean not found when inject %s.%s for bean '%s': %s", beanClass.getSimpleName(),
                        accObjName, def.getName(), def.getBeanClass().getName()));
            }
            if (depends != null) {
                if (field != null) {
                    logger.info("Field injection: {}.{} = {}", def.getBeanClass().getName(), accObjName, depends);
                    field.set(instance, depends);
                }
                if (method != null) {
                    logger.info("Mield injection: {}.{} ({})", def.getBeanClass().getName(), accObjName, depends);
                    method.invoke(instance, depends);
                }

            }
        }
    }

    /**
     * 检查是否合法的字段或方法
     *
     * @param member
     */
    private void checkFieldOrMethod(Member member) {
        int modifiers = member.getModifiers();
        if (Modifier.isStatic(modifiers)) {
            throw new BeanDefinitionException("Cannot inject static field: " + member);
        }
        if (Modifier.isFinal(modifiers)) {
            if (member instanceof Field) {
                throw new BeanDefinitionException("Cannot inject final field: " + member);
            }
            if (member instanceof Method) {
                logger.warn("Inject final method should be careful because it is not called on target bean when bean is proxied and may cause NullPointerException.");
            }
        }
    }

    private <T> T findBean(Class<T> requiredType) {
        BeanDefinition definition = findBeanDefinition(requiredType);
        if (definition == null) {
            return null;
        }
        return (T) definition.getRequiredInstance();
    }

    private <T> T findBean(String name, Class<T> requiredType) {
        BeanDefinition definition = findBeanDefinition(name, requiredType);
        if (definition == null) {
            return null;
        }
        return (T) definition.getRequiredInstance();
    }

    /**
     * 普通bean的init/destroy method可能不为空,工厂bean的init/destroy method name 可能不为空
     * 回调方法并执行
     *
     * @param instance
     * @param method
     * @param methodName
     */
    private void callMethod(Object instance, Method method, String methodName) {
        if (method != null) {
            try {
                method.invoke(instance);
            } catch (ReflectiveOperationException e) {
                throw new BeanCreationException(e);
            }
        } else if (methodName != null) {
            Method namedMethod = ClassUtils.getNamedMethod(instance.getClass(), methodName);
            namedMethod.setAccessible(true);
            try {
                namedMethod.invoke(instance);
            } catch (ReflectiveOperationException e) {
                throw new BeanCreationException(e);
            }
        }
    }

    //endregion

    //region ioc
    @Override
    public boolean containsBean(String name){
        return this.beans.containsKey(name);
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType){
        T bean = findBean(name, requiredType);
        if(bean==null){
            throw new NoSuchBeanDefinitionException(String.format("No bean defined with name '%s' and type '%s'.", name, requiredType));
        }
        return bean;
    }

    @Override
    public void close(){
        logger.info("Closing {}...", this.getClass().getName());
        //遍历所有bean执行销毁方法
        this.beans.values().forEach(beanDefinition -> {
            Object beanInstance = getProxiedInstance(beanDefinition);
            callMethod(beanInstance,beanDefinition.getDestroyMethod(),beanDefinition.getDestroyMethodName());
        });
        this.beans.clear();
        logger.info("{} closed.", this.getClass().getName());
        ApplicationContextUtils.setApplicationContext(null);
    }
}
