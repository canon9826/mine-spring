package com.study.spring.jdbc.transaction;

import javax.sql.DataSource;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * TransactionalBeanPostProcessor 继承 @code{AnnotationProxyBeanPostProcessor<Transactional>}
 *  AnnotationProxyBeanPostProcessor会在创建Transactional的Bean后创建Transactional.value()的代理类->PlatformTransactionManager
 *      PlatformTransactionManager被DataSourceTransactionManager实现
 *          创建DataSourceTransactionManager对象
 */
public class DataSourceTransactionManager implements PlatformTransactionManager, InvocationHandler {

    //ThreadLocal->
    //它为每个使用该变量的线程都提供了一个独立的副本，从而每个线程都可以独立地改变自己的副本，而不会影响其他线程的副本。
    // 简而言之，ThreadLocal 实现了线程级别的数据隔离，非常适用于在多线程环境下各线程需要拥有独立的变量副本的场景。
    //保存数据库连接
    static final ThreadLocal<TransactionStatus> transactionStatus = new ThreadLocal<>();

    final DataSource dataSource;


    public DataSourceTransactionManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * 执行Transactional注解的方法时的真实执行
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //获取线程变量中的数据库连接
        TransactionStatus ts = transactionStatus.get();
        if(ts == null){
            //当前无事务,开启事务
            try(Connection connection = dataSource.getConnection()){
                final boolean autoCommit = connection.getAutoCommit();
                if(autoCommit){
                    connection.setAutoCommit(false);
                }
                try{
                    //设置ThreadLocal状态
                    transactionStatus.set(new TransactionStatus(connection));
                    //业务方法
                    Object res = method.invoke(proxy, args);
                    //提交事务
                    connection.commit();
                    return res;
                }catch (InvocationTargetException e){
                    //执行错误,回滚
                    TransactionException te = new TransactionException(e.getCause());
                    try{
                        connection.rollback();
                    }catch (SQLException sqlE){
                        te.addSuppressed(sqlE);
                    }
                    throw te;
                }finally {
                    //清除ThreadLocal状态
                    transactionStatus.remove();
                    if(autoCommit){
                        connection.setAutoCommit(true);
                    }
                }
            }
        }else{
            // 当前已有事务,加入当前事务执行
            return method.invoke(proxy,args);
        }
    }
}
