package com.study.spring.jdbc.transaction;

import java.lang.reflect.InvocationTargetException;

public class TransactionException extends InvocationTargetException {
    public TransactionException() {
    }

    public TransactionException(Throwable target) {
        super(target);
    }

    public TransactionException(Throwable target, String s) {
        super(target, s);
    }
}
