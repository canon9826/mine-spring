/**
 * @ClassName ClassPathUtils
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/1 20:15
 * @Version 1.0
 **/

package com.study.spring.yaml;

import com.study.spring.resource.ResourceResolver;

import java.io.*;
import java.nio.charset.StandardCharsets;


public class ClassPathUtils {
    /**
     * 获取路径对应的资源文件，转换为输入流
     * @param path
     * @param callback
     * @param <T>
     * @return
     */
    public static <T> T readInputStream(String path, InputStreamCallback<T> callback) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        //获取资源文件作为输入流
        try(InputStream input = new ResourceResolver(null).getContextClassLoader().getResourceAsStream(path)){
            if(input == null){
                throw new FileNotFoundException("File not found in classpath:"+path);
            }
            return callback.doWithInputStream(input);
        }catch (IOException e){
            e.printStackTrace();
            throw new UncheckedIOException(e);
        }
    }


    /**
     * 没用到啊好像
     * @param path
     * @return
     */
    public static String readString(String path){
        return readInputStream(path,(input)->{
            byte[] data = readAllBytes(input);
            return new String(data, StandardCharsets.UTF_8);
        });
    }

    public static byte[] readAllBytes(InputStream inputStream)  {
        int bufLen = 1024 * 8;
        byte[] buf = new byte[bufLen];
        int readLen;
        try {
            final BufferedInputStream bis = new BufferedInputStream(inputStream);
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((readLen = bis.read(buf)) != -1) {
                baos.write(buf,0,readLen);
                return baos.toByteArray();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
