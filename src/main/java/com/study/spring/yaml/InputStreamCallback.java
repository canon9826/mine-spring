/**
 * @InterfaceName InputStreamCasllback
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/4/1 20:34
 * @Version 1.0
 **/

package com.study.spring.yaml;

import java.io.IOException;
import java.io.InputStream;

@FunctionalInterface
public interface InputStreamCallback<T> {

    T doWithInputStream(InputStream stream) throws IOException;
}
