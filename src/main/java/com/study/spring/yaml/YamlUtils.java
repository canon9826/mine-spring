/**
 * @ClassName YamlUtils
 * @Description
 * @Author Canon
 * @Email canon9826@sina.com
 * @Date 2024/4/1 11:12
 * @Version 1.0
 **/

package com.study.spring.yaml;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.representer.Representer;
import org.yaml.snakeyaml.resolver.Resolver;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class YamlUtils {

    public static Map<String,Object> loadYamlAsPlainMap(String path){
        Map<String, Object> data = loadYaml(path);
        Map<String, Object> plain = new LinkedHashMap<>();
        //解析yaml
        convertTo(data, "", plain);
        return plain;
    }

    /**
     *
     * @param path
     * @return
     */
    private static Map<String, Object> loadYaml(String path) {
        LoaderOptions loaderOptions = new LoaderOptions();
        DumperOptions dumperOptions = new DumperOptions();
        Representer representer = new Representer(dumperOptions);
        Resolver resolver = new NoImplicitResolver();
        Yaml yaml = new Yaml(new Constructor(loaderOptions), representer, dumperOptions, resolver);
        return ClassPathUtils.readInputStream(path, (input) -> {
            return (Map<String, Object>) yaml.load(input);
        });

    }

    /**
     * 处理yaml的map
     * @param source
     * @param prefix
     * @param plain
     */
    private static void convertTo(Map<String, Object> source, String prefix, Map<String, Object> plain) {
        for (String key : source.keySet()) {
            Object value = source.get(key);
            //value是一个map,递归合并多级key
            if (value instanceof Map) {
                Map<String, Object> subMap = (Map<String, Object>) value;
                convertTo(subMap, prefix + key + ".", plain);
            } else
                //value是一个list,直接存入结果
                if (value instanceof List) {
                plain.put(prefix + key, value);
            } else {
                //存入结果的字符串
                plain.put(prefix + key, value.toString());
            }
        }
    }


    /**
     * YAML时禁用隐式类型转换
     * 只有明确标记的类型才会被解析
     */
    static class NoImplicitResolver extends Resolver {

        public NoImplicitResolver() {
            super();
            super.yamlImplicitResolvers.clear();
        }
    }
}
