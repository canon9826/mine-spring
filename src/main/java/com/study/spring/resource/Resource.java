package com.study.spring.resource;

import java.util.Objects;

/**
 * @ClassName com.study.spring.io.Resource
 * @Decsription 表示文件
 * @Author GuoJunJie
 * @Date 2024/3/17 19:37
 * @Version 1.0
 **/

public final class Resource {
    public final String path;
    public final String name;

    public Resource(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public String path() {
        return path;
    }

    public String name() {
        return name;
    }

    @Override
    public String toString() {
        return "com.study.spring.io.Resource{" +
                "path='" + path + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Resource)) return false;
        Resource resource = (Resource) o;
        return Objects.equals(path, resource.path) && Objects.equals(name, resource.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, name);
    }
}
