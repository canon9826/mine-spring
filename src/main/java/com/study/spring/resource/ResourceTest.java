/**
 * @ClassName ResouceTest
 * @Decsription
 * @Author GuoJunJie
 * @Date 2024/3/27 22:30
 * @Version 1.0
 **/

package com.study.spring.resource;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class ResourceTest {
    public static void main(String[] args) throws IOException, URISyntaxException {
        //ResourceResolver.scanByStack测试
        //类文件
        //List<Class<?>> classes = new ResourceResolver(null).scanByStack("com.study.spring");
        //System.out.println(classes);
        // jar包
        //List<Class<?>> classes = new ResourceResolver(null).scanByStack("com.mysql.jdbc");
        //System.out.println(classes);


        //ResourceResolver.scan测试
        //jar包
        //ResourceResolver resourceResolver = new ResourceResolver("com.mysql.jdbc");
        //类文件
        //ResourceResolver resourceResolver = new ResourceResolver("com.study.spring");
        //List<String> list1 =resourceResolver.scan(res->{
        //    String name = res.name();
        //    if(name.endsWith(".class")){
        //        return name.replace('/','.').replace(File.separatorChar,'.');
        //    }
        //    return null;
        //});
        //System.out.println(list1);


        //ResourceResolver.mineScan测试
        //jar
        //ResourceResolver resourceResolver = new ResourceResolver("com.mysql.jdbc");
        //类文件
        ResourceResolver resourceResolver = new ResourceResolver("com.study.spring");
        List<String> list2 = resourceResolver.mineScan(res->{
            String name = res.name();
            if(name.endsWith(".class")){
                return name.replace('/','.').replace(File.separatorChar,'.');
            }
            return null;
        });

        System.out.println(list2);
    }
}
