package com.study.spring.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * @ClassName com.study.spring.io.ResourceResolver
 * @Decsription 资源文件解析器
 * @Author GuoJunJie
 * @Date 2024/3/17 19:58
 * @Version 1.0
 **/


public class ResourceResolver {
    Logger logger = LoggerFactory.getLogger(getClass());

    private String basePackage;

    public ResourceResolver(String basePackage) {
        this.basePackage = basePackage;
    }

    /**
     * 扫描路径下所有文件,function方便处理返回结果
     * @param mapper
     * @param <R>
     * @return
     */
    public <R> List<R> scan(Function<Resource,R> mapper){
        String basePackagePath = this.basePackage.replace(".", "/");
        String path = basePackagePath;
        try {
            List<R> collector = new ArrayList<>();
            scan0(basePackagePath, path, collector, mapper);
            return collector;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param basePackagePath
     * @param path
     * @param collector
     * @param mapper
     * @param <R>
     * @throws IOException
     * @throws URISyntaxException
     */
    <R> void scan0(String basePackagePath, String path, List<R> collector, Function<Resource, R> mapper) throws IOException, URISyntaxException {
        logger.atDebug().log("scan path: {}", path);
        //通过对应classLoader获取path对应的资源的URL对象的枚举
        Enumeration<URL> en = getContextClassLoader().getResources(path);
        while (en.hasMoreElements()) {
            //处理资源
            URL url = en.nextElement();
            URI uri = url.toURI();
            String uriStr = removeTrailingSlash(uriToString(uri));
            //得到扫描的根路径
            String uriBaseStr = uriStr.substring(0, uriStr.length() - basePackagePath.length());
            if (uriBaseStr.startsWith("file:")) {
                uriBaseStr = uriBaseStr.substring(5);
            }
            //扫描筛选文件并加入结果
            if (uriStr.startsWith("jar:")) {
                scanFile(true, uriBaseStr, jarUriToPath(basePackagePath, uri), collector, mapper);
            } else {
                scanFile(false, uriBaseStr, Paths.get(uri), collector, mapper);
            }
        }
    }

    /**
     * ClassLoader首先从Thread.getContextClassLoader()获取，如果获取不到，再从当前Class获取，
     * 因为Web应用的ClassLoader不是JVM提供的基于Classpath的ClassLoader，而是Servlet容器提供的ClassLoader，
     * 它不在默认的Classpath搜索，而是在/WEB-INF/classes目录和/WEB-INF/lib的所有jar包搜索，
     * 从Thread.getContextClassLoader()可以获取到Servlet容器专属的ClassLoader；
     * @return
     */
    public ClassLoader getContextClassLoader() {
        ClassLoader classLoader = null;
        //当前线程关联的类加载器
        classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader == null) {
            //初始化类的类加载器
            classLoader = getClass().getClassLoader();
        }
        return classLoader;
    }

    /**
     * 创建uri的文件系统,定位path路径
     * @param basePackagePath
     * @param jarUri
     * @return
     * @throws IOException
     */
    Path jarUriToPath(String basePackagePath, URI jarUri) throws IOException {
        return FileSystems.newFileSystem(jarUri, Collections.emptyMap()).getPath(basePackagePath);
    }

    /**
     * 从根路径扫描文件,根据mapper筛选处理文件,放入collector结果集
     * @param isJar
     * @param base
     * @param root
     * @param collector
     * @param mapper
     * @param <R>
     * @throws IOException
     */
    <R> void scanFile(boolean isJar, String base, Path root, List<R> collector, Function<Resource, R> mapper) throws IOException {
        String baseDir = removeTrailingSlash(base);
        Files.walk(root).filter(Files::isRegularFile).forEach(file -> {
            Resource res = null;
            if (isJar) {
                res = new Resource(baseDir, removeLeadingSlash(file.toString()));
            } else {
                String path = file.toString();
                String name = removeLeadingSlash(path.substring(baseDir.length()));
                res = new Resource("file:" + path, name);
            }
            //System.out.println(res.toString());
            logger.atDebug().log("found resource: {}", res);
            //执行scan方法传入的mapper操作
            R r = mapper.apply(res);
            if (r != null) {
                collector.add(r);
            }
        });
    }

    /**
     * 获取URI的地址转化为String形式,以UTF8的编码方式
     * @param uri
     * @return
     * @throws UnsupportedEncodingException
     */
    String uriToString(URI uri) throws UnsupportedEncodingException {
        return URLDecoder.decode(uri.toString(), StandardCharsets.UTF_8.name());
    }

    /**
     * 去除地址开始的路径分隔符
     * @param s
     * @return
     */
    String removeLeadingSlash(String s) {
        if (s.startsWith("/") || s.startsWith("\\")) {
            s = s.substring(1);
        }
        return s;
    }

    /**
     * 去除地址结尾的路径分隔符
     * @param s
     * @return
     */
    String removeTrailingSlash(String s) {
        if (s.endsWith("/") || s.endsWith("\\")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }


    //-------------------------------------stackOverFlow的方法-------------------------------------------------


    public  List<Class<?>> scanByStack(String pkgName) throws IOException, URISyntaxException {
        // 将传入包的点转为路径分隔符
        // java读取资源文件时,采用'/'作为路径分隔符,windows,unix,jar包中都是如此
        final String pkgPath = pkgName.replace('.', '/');
        // 从项目加载系统类的路径下寻找资源
        // 从当前文件的路径下寻找资源this.getClass().getResource()
        final URI pkg = Objects.requireNonNull(ClassLoader.getSystemClassLoader().getResource(pkgPath)).toURI();
        final ArrayList<Class<?>> allClasses = new ArrayList<Class<?>>();

        Path root;
        if (pkg.toString().startsWith("jar:")) {
            //默认情况下并不会预先注册JAR包中的文件作为文件系统
            try {
                root = FileSystems.getFileSystem(pkg).getPath(pkgPath);
            } catch (final FileSystemNotFoundException e) {
                // 使用 newFileSystem() 来打开一个指向JAR文件的URI，并从中创建一个新的内存中文件系统视图，
                // newFileSystem()用于非默认文件系统，如ZIP、JAR文件或其他支持的URI类型
                root = FileSystems.newFileSystem(pkg, Collections.emptyMap()).getPath(pkgPath);
            }
        } else {
            //直接获取path,会返回对应操作系统的路径
            root = Paths.get(pkg);
        }

        final String extension = ".class";
        //以root为起始路径遍历
        try (final Stream<Path> allPaths = Files.walk(root)) {
            //是否常规文件(存储有数据的文件)
            allPaths.filter(Files::isRegularFile).forEach(file -> {
                try {
                    final String path;
                    if (pkg.toString().startsWith("jar:")) {
                        //file:/com/mysql/jdbc/SocketFactoryWrapper.class
                        path = file.toString().replace('/', '.');
                    }else{
                        //E:\workspace\StudyWorkspace\mine-spring\target\classes\com\study\spring\MineSpringApplication.class
                       path = file.toString().replace(File.separatorChar, '.');
                    }
                    //path:.com.mysql.jdbc.SocketFactoryWrapper.class
                    final String name = path.substring(path.indexOf(pkgName), path.length() - extension.length());
                    //name:com.mysql.jdbc.SocketFactoryWrapper
                    allClasses.add(Class.forName(name));
                } catch (final ClassNotFoundException | StringIndexOutOfBoundsException ignored) {
                }
            });
        }
        return allClasses;
    }


    //---------------------------------------------------------mineScan----------------------------------------------------------------

    public <R> List<R> mineScan(Function<Resource, R> mapper) throws URISyntaxException, IOException {
        List<R> collection = new ArrayList<>();
        ClassLoader loader = Optional.ofNullable(Thread.currentThread().getContextClassLoader()).orElse(ClassLoader.getSystemClassLoader());
        String pkgPath = basePackage.replace('.', '/');
        URI resource = loader.getResource(pkgPath).toURI();
        Path root;
        if (resource.toString().startsWith("jar")) {
            try {
                root = FileSystems.getFileSystem(resource).getPath(pkgPath);
            } catch (Exception e) {
                root = FileSystems.newFileSystem(resource, Collections.emptyMap()).getPath(pkgPath);
            }
        } else {
            root = Paths.get(resource);
        }

        Path finalRoot = root;
        Files.walk(root).filter(Files::isRegularFile).forEach(file -> {
            Resource source = path2Resource(resource.toString(), file.toString(), pkgPath, finalRoot.toString());
            R apply = mapper.apply(source);
            if (Objects.nonNull(apply)) {
                collection.add(apply);
            }
        });
        return collection;
    }

    Resource path2Resource(String resource, String file, String pkgPath, String root) {
        String path, filePath;

        if (resource.startsWith("jar")) {
            path = resource.replace(root, "");
            filePath = file;
        } else {
            String pathIndex = pkgPath.replace('/', File.separatorChar);
            path = root.substring(0, root.indexOf(pathIndex));
            filePath = file.substring(file.indexOf(pathIndex));
        }

        if (path.endsWith("/") || path.endsWith(File.separator)) {
            path = path.substring(0, path.length() - 1);
        }
        if(filePath.startsWith("/")||filePath.startsWith(File.separator)){
            filePath = filePath.substring(1);
        }

        return new Resource(path, filePath);
    }
}
